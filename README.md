# transportes-movil

# Levantar 

## PRIMERA VEZ Y AL INSTALAR NUEVAS LIBRERIAS
1. ionic build  
2. ionic capacitor add android --livereload-url=http://<IP-LOCAL>:8100 
3. npx cap copy 
4. npx cap sync 
5. npx cap open android 
    ```text
        agregar linea en el AndroidManifest en android studio
    ```
    ```xml
       <application
       android:usesCleartextTraffic="true"/>
    ```
6. ionic serve --address <IP-LOCAL>
7. ionic capacitor run android --livereload-url=http://<IP-LOCAL>:8100

## CUANDO ESTA TODO CONFIGURADO
1. ionic serve --address <IP-LOCAL>
2. ionic capacitor run android --livereload-url=http://<IP-LOCAL>:8100

`Tip Verga`
> Si no se falla el sync del graddle dar clic en ```try again```
