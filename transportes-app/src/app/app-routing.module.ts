import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {RUTAS_PAGINAS_AUTH} from './submodulos/submodulo-auth0-movil/constantes/rutas-pagina-auth';
const routes: Routes = [
    {path: '', redirectTo: 'tabs', pathMatch: 'full'},
    {path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule'},
    {
        path: 'locales-mapa',
        loadChildren: './rutas/locales-mapa/locales-mapa.module#LocalesMapaPageModule'
    },
    ...RUTAS_PAGINAS_AUTH,  { path: 'paradas', loadChildren: './rutas/paradas/paradas.module#ParadasPageModule' },

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
