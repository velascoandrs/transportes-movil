import {Component} from '@angular/core';
import {AlertController, MenuController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import * as moment from 'moment';
import 'moment/locale/es';
import {environment} from '../environments/environment';
import {
    OneSignal,
    OSPermissionSubscriptionState
} from '@ionic-native/onesignal/ngx';
import {DispositivoMovilRestService} from './submodulos/submodulo-auth0-movil/servicios/rest/dispositivo-movil-rest.service';
import {Auth0Service} from './submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service';
import {LocalStorageService} from './submodulos/submodulo-auth0-movil/servicios/local-storage/local-storage.service';
import {Router} from '@angular/router';
import {ScreenOrientation} from '@ionic-native/screen-orientation/ngx';
import {VersionAplicativoRestService} from './submodulos/submodulo-auth0-movil/servicios/rest/version-aplicativo-rest.service';

moment.locale('es');


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    noHayNuevaVersion = false;
    hayNuevaVersion = false;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private _screenOrientation: ScreenOrientation,
        private menu: MenuController,
        private readonly _oneSignal: OneSignal,
        private readonly _dispositivoMovilRestService: DispositivoMovilRestService,
        private readonly _auth0Service: Auth0Service,
        private readonly _localStorage: LocalStorageService,
        private readonly router: Router,
        private readonly _versionAplicativoRestService: VersionAplicativoRestService,
        public alertController: AlertController,
    ) {
        this._screenOrientation.lock(this._screenOrientation.ORIENTATIONS.PORTRAIT);
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(async () => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.verificarVersionDelAplicativo().then().catch();
            try {
                await this.escucharPushNotifications();
                if (this._localStorage.getItem('estaLogeado')) {
                    this._auth0Service.usuario = this._localStorage.getItem('usuario');
                    this._auth0Service.estaLogeado = this._localStorage.getItem(
                        'estaLogeado'
                    );
                    const tokenId = this._auth0Service.estaLogeado.token.id_token;
                    this._auth0Service.establecerToken(tokenId);
                    this._auth0Service.revisarTiempoToken();
                } else {
                    this.router.navigate(['/tabs', 'login']);
                }
            } catch (error) {
                console.error({
                    error,
                    mensaje: 'Error creando dispositivo y push notifications'
                });
            }
        });
    }


    openFirst() {
        this.menu.enable(true, 'first');
        this.menu.open('first');
    }

    openEnd() {
        this.menu.open('end');
    }

    async escucharPushNotifications() {
        try {
            this._oneSignal.startInit(
                environment.oneSignal.appId,
                environment.oneSignal.googleProjectNumber
            );
            this._oneSignal.inFocusDisplaying(
                this._oneSignal.OSInFocusDisplayOption.InAppAlert
            );
            this._oneSignal.handleNotificationReceived().subscribe(() => {
                console.log('Se recibió una notificación.');
            });

            this._oneSignal.handleNotificationOpened().subscribe(() => {
                console.log('Se abrio una notificación.');
            });
            this._oneSignal.registerForPushNotifications();
            this._oneSignal.endInit();
            let respuesta: OSPermissionSubscriptionState;
            try {
                respuesta = await this._oneSignal.getPermissionSubscriptionState();
            } catch (error) {
                console.error({
                    error,
                    mensaje: 'Error obteniendo permiso de suscripcion'
                });
            }
            if (respuesta) {
                const dispositivo = {
                    playerID: respuesta.subscriptionStatus.userId,
                    habilitado: 1,
                };
                try {
                    await this._dispositivoMovilRestService.create(dispositivo);
                } catch (error) {
                    console.error({
                        error,
                        mensaje: 'Puede que ya este creado'
                    });
                }
            }
        } catch (error) {
            console.error({
                error,
                mensaje: 'Error al registrar push notifications'
            });
        }
    }

    async verificarVersionDelAplicativo() {
        try {
            const esAndroid = this.platform.is('android');
            const esiOS = this.platform.is('ios');
            const respuesta = await this._versionAplicativoRestService.findAll();
            const versionActual = respuesta[0][0];
            if (versionActual) {
                let puedeUsar = false;
                if (esAndroid) {
                    puedeUsar =
                        (
                            versionActual.versionActual === environment.versionActualAndroid ||
                            versionActual.versionActualTiendas === environment.versionActualAndroid
                        );
                }
                if (esiOS) {
                    puedeUsar =
                        (
                            versionActual.versionActualiOS === environment.versionActualiOS ||
                            versionActual.versionActualiOSTiendas === environment.versionActualiOS
                        );
                }
                if (puedeUsar) {
                    console.log({
                        mensaje: 'Puede usar el aplicativo',
                    });
                    if (this.noHayNuevaVersion) {

                    } else {
                        this.noHayNuevaVersion = true;
                        this.hayNuevaVersion = false;
                        setInterval(
                            async () => {
                                this.verificarVersionDelAplicativo().then().catch();
                            }, 60000
                        );
                    }

                } else {
                    const alert = await this.alertController.create({
                        header: 'NUEVA ACTUALIZACIÓN',
                        subHeader: 'Actualización',
                        message: 'Hay una nueva actualización disponible.'
                    });
                    await alert.present();
                    setTimeout(
                        () => {
                            if (esAndroid) {
                                window.location.href = environment.linkInstalacionAndroid;
                            }
                            if (esiOS) {
                                window.location.href = environment.linkInstalacionIos;
                            }
                        },
                        1000
                    );
                    if (this.hayNuevaVersion) {

                    } else {
                        this.noHayNuevaVersion = false;
                        this.hayNuevaVersion = true;
                        setInterval(
                            async () => {
                                this.verificarVersionDelAplicativo().then().catch();
                            }, 5000
                        );
                    }
                }
            }
        } catch (error) {
            console.error({
                error,
                mensaje: 'Error conectando con el servidor.',
            });

        }
    }

}
