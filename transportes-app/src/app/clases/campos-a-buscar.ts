import {CamposABuscarInterface} from '../interfaces/rest/campos-a-buscar.interface';

export declare class CamposABuscar implements CamposABuscarInterface {
    campo: string;
    valor: any;

    constructor(objeto: CamposABuscarInterface);
}
