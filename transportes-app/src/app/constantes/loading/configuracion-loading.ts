export const CONFIGURACION_LOADING = {
    spinner: 'dots',
    duration: 5000,
    message: 'Cargando...',
    translucent: true,
    cssClass: 'custom-class custom-loading'
};
