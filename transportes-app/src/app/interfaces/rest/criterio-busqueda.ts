import {FindWhereRelations} from './find-where-or-relations';
import {CamposABuscarInterface} from './campos-a-buscar.interface';

export interface CriterioBusquedaFindWhereOr {
    camposABuscar: CamposABuscarInterface[];
    skip: number;
    take: number;
    relations?: FindWhereRelations[];
}
