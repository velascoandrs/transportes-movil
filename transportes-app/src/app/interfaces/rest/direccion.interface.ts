import {EdificioInterface} from './edificio.interface';
import {DatosUsuarioInterface} from '../../submodulos/submodulo-auth0-movil/interfaces/datos-usuario.interface';
import {LocalizacionInterface} from './localizacion.interface';

export interface DireccionInterface {
    id?: number;

    numeroCalle?: string;

    callePrincipal?: string;

    calleSecundaria?: string;

    nombreEdificio?: string;

    piso?: string;

    sector?: string;

    referencia?: string;

    edificio?: EdificioInterface | number | string;

    datosUsuario?: DatosUsuarioInterface | number;

    codigoPostal?: string;
    createdAt?: string;
    updatedAt?: string;

    habilitado?: 0 | 1;
    localizacion?: LocalizacionInterface;

    lugar?: number | string;
}
