import {EmpresaInterface} from './empresa.interface';
import {DireccionInterface} from './direccion.interface';
import {EstablecimientoInterface} from './establecimiento.interface';

export interface EdificioInterface {
    id?: number;
    nombre?: string;
    habilitado?: number | boolean;
    esMatriz?: number | boolean;
    empresa?: EmpresaInterface | number | string;
    direccion?: DireccionInterface | number | string;
    telefono?: string;
    whatsapp?: string;
    nombreResponsable?: string;
    extension?: string;
    createdAt: string;
    updatedAt: string;
    establecimientos?: EstablecimientoInterface[];
    // ruta?: RutaInterface | number | string;
    // ediCliRuta?: EdiCliRutaInterface | any;
}

