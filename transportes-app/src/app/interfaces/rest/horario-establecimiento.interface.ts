import {EstablecimientoInterface} from './establecimiento.interface';
import {HorarioInterface} from './horario.interface';

export interface HorarioEstablecimientoInterface {
    id?: number;
    createdAt?: string;
    updatedAt?: string;
    habilitado?: 0 | 1;
    horario?: HorarioInterface | number;
    establecimiento?: EstablecimientoInterface | number;
    esHorarioEspecial?: 0 | 1;
    puedeAtender?: 0 | 1;
}
