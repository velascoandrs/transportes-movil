export interface LocalizacionInterface {
    localizacion: {
        type: 'Polygon' | 'Point';
        coordinates: number[] | number[][][]
    };
    entidadNombre: string;
    entidadId: string;
    id?: string;
    createdAt?: string;
}
