export interface ObtenerEdificioConsulta {
    coordenadas: number[];
    distanciaMaxima: number;
    distanciaMinima: number;
    entidadNombre: 'edificio';
    mostrarEmpresa: 1 | 0;
    tipoCoordenadas: 'Point';
}
