export interface SlideMenuInterface {
  nombre?: string;
  descripcion?: string;
  habilitado?: 0 | 1;
}
