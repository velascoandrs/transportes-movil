import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-modal-mostrar-opciones',
  templateUrl: './modal-mostrar-opciones.component.html',
  styleUrls: ['./modal-mostrar-opciones.component.scss'],
})
export class ModalMostrarOpcionesComponent implements OnInit {

  constructor(
      public modalController: ModalController,
  ) { }

  ngOnInit() {}

  async dismiss() {
    await this.modalController.dismiss(undefined);
  }

  async irVerMenu() {
    await this.modalController.dismiss('seccion-menu');
  }

}
