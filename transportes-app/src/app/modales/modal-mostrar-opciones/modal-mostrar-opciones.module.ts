import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModalMostrarOpcionesComponent} from './modal-mostrar-opciones.component';
import {IonicModule} from '@ionic/angular';


@NgModule({
    declarations: [
        ModalMostrarOpcionesComponent
    ],
    imports: [
        CommonModule,
        IonicModule
    ],
    entryComponents: [
        ModalMostrarOpcionesComponent
    ]
})
export class ModalMostrarOpcionesModule {
}
