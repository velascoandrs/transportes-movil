import {Component, Input, OnInit} from '@angular/core';
import {ModalController, ToastController} from '@ionic/angular';
import {EdificioInterface} from '../../interfaces/rest/edificio.interface';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'mostrar-informacion-modal',
    templateUrl: './mostrar-informacion.html',
})
// tslint:disable-next-line:component-class-suffix
export class ModalMostrarInformacion {

    @Input() edificio: EdificioInterface;
    @Input() coordenadas: {
        latitud: number,
        longitud: number
    };
    urlGoogle = environment.urlGoogleCloudStorage + ':' + environment.portGoogleCloudStorage;

    constructor(public toastController: ToastController,
                public modalController: ModalController) {

    }

    dismiss() {
        this.modalController.dismiss(undefined);
    }

    async mostrarToast(texto: string, color: string = 'danger', tiempo: number = 2000) {
        const toast = await this.toastController.create({
            message: texto,
            duration: tiempo,
            color
        });
        toast.present();
        return null;
    }



}
