import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {LocalesMapaPage} from './locales-mapa.page';
import {ModalMostrarInformacion} from '../../modales/mostrar-informacion/mostrar-informacion';
import {MostrarDireccionModule} from "../../submodulos/submodulo-auth0-movil/componentes/mostrar-direccion/mostrar-direccion.module";

const routes: Routes = [
    {
        path: '',
        component: LocalesMapaPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        MostrarDireccionModule,
    ],
    declarations: [
        LocalesMapaPage,
        ModalMostrarInformacion
    ],
    entryComponents: [
        ModalMostrarInformacion
    ]
})
export class LocalesMapaPageModule {
}
