import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalesMapaPage } from './locales-mapa.page';

describe('LocalesMapaPage', () => {
  let component: LocalesMapaPage;
  let fixture: ComponentFixture<LocalesMapaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalesMapaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalesMapaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
