import {Component, OnInit} from '@angular/core';
import {ModalInfoLocalComponent} from '../../modales/modal-info-local/modal-info-local.component';
import {EstablecimientoRestService} from '../../servicios/rest/establecimiento-rest.service';
import {ModalController, Platform} from '@ionic/angular';
import {EstablecimientoInterface} from '../../interfaces/rest/establecimiento.interface';
import {CargandoService} from '../../servicios/cargando-service/cargando.service';
import {ToasterService} from '../../servicios/toaster-service/toaster.service';
import {PAGINACION} from '../../constantes/paginacion/paginacion';
import {MENSAJE_ERROR} from '../../constantes/mensajes/mensaje-error';
import {EdificioInterface} from '../../interfaces/rest/edificio.interface';
import {DeviceInfo} from '@capacitor/core';
import {InicializarMapa} from '../../servicios/openlayers/interfaces/inicializar-mapa';
import {OpenlayersService} from '../../servicios/openlayers/openlayers.service';
import {ObjetoEventoClickOpenLayer} from '../../servicios/openlayers/interfaces/objeto-click-open-layer';
import {Plugins} from '@capacitor/core';
import {EdificioService} from '../../servicios/rest/edificio.service';
import {MarcadorImagenOpenLayer} from '../../servicios/openlayers/interfaces/marcador-imagen-open-layer';
import {DireccionInterface} from '../../interfaces/rest/direccion.interface';
import {ModalMostrarInformacion} from '../../modales/mostrar-informacion/mostrar-informacion';
import {environment} from '../../../environments/environment';


const {Device} = Plugins;
const {Geolocation} = Plugins;

declare var ol: any;

@Component({
    selector: 'app-locales-mapa',
    templateUrl: './locales-mapa.page.html',
    styleUrls: ['./locales-mapa.page.scss'],
})
export class LocalesMapaPage implements OnInit {
    mostrarMapa = true;
    mostrarLista = false;
    establecimientos: EstablecimientoInterface[] = [];
    edificios: EdificioInterface[] = [];
    skip = PAGINACION.skip;
    take = 1000;
    hayMasLocales: boolean;
    map: any;
    versionAplicativo = '';

    constructor(
        private readonly _establecimientosRestService: EstablecimientoRestService,
        public modalController: ModalController,
        private readonly _cargandoService: CargandoService,
        private readonly _toastService: ToasterService,
        private readonly _openlayersService: OpenlayersService,
        private readonly _edificioService: EdificioService,
        private platform: Platform,
    ) {
    }

    async ngOnInit() {
        const esAndroid = this.platform.is('android');
        const esiOS = this.platform.is('ios');
        if (esAndroid) {
            this.versionAplicativo = environment.versionActualAndroid;
        }
        if (esiOS) {
            this.versionAplicativo = environment.versionActualiOS;
        }
        this.cambiarTab('lista');
        console.log(this.establecimientos);
    }

    async cargarMapa() {
        await this._cargandoService.habilitarCargandoService('Cargando mapa...');
        try {
            const coordenadas = await Geolocation.getCurrentPosition();
            const device = await Device.getInfo();
            this.edificios = await this._edificioService.obtenerEdificios({
                coordenadas: [coordenadas.coords.latitude, coordenadas.coords.longitude],
                distanciaMaxima: 10000,
                distanciaMinima: 1,
                entidadNombre: 'edificio',
                mostrarEmpresa: 0,
                tipoCoordenadas: 'Point'
            });
            this.edificios = this.edificios
                .filter(
                    (edificio) => {
                        return edificio.establecimientos.some((e) => e.soloDelivery === 0);
                    }
                );
            await this.inicializarMapa(coordenadas.coords.latitude, coordenadas.coords.longitude, device);
            await this._cargandoService.deshabilitarCargandoService();
        } catch (e) {
            await this._cargandoService.deshabilitarCargandoService();
        }

    }

    cambiarTab(tabSeleccionado: 'mapa' | 'lista') {

        if (tabSeleccionado === 'mapa' && !this.mostrarMapa) {
            this.mostrarMapa = true;
            this.mostrarLista = false;
            this.cargarMapa().then().catch();
        }
        if (tabSeleccionado === 'lista' && !this.mostrarLista) {
            this.mostrarMapa = false;
            this.mostrarLista = true;
            this.buscarLocales();
        }
    }

    async buscarLocales() {
        let locales: any = [[], 0];

        try {
            await this._cargandoService.habilitarCargandoService('Buscando locales...');
            const criterioBusqueda = {
                where:
                    {
                        habilitado: 1,
                        soloDelivery: 0,
                    },
                skip: this.skip * this.take,
                take: this.take,
                relations: [
                    'edificio',
                    'edificio.direccion',
                    'horariosEstablecimiento',
                    'horariosEstablecimiento.horario'
                ]
            };
            locales = await this._establecimientosRestService
                .findAll(criterioBusqueda);
            this.establecimientos.push(...locales[0]);
            const noHayLocales = locales[0].length === 0;
            const encontroLocales = locales[0].length > 0;
            await this._cargandoService.deshabilitarCargandoService();

            // if (noHayLocales) {
            //     this._toastService.mostrarToast('No hay mas locales');
            // }
            if (encontroLocales) {
                this.skip = this.skip + 1;
            }
            if (noHayLocales) {
                this.hayMasLocales = false;
            }
        } catch (e) {
            console.log(e);
            await this._toastService.mostrarToast(MENSAJE_ERROR);
            await this._cargandoService.deshabilitarCargandoService();
        }
    }

    async mostrarInfoLocal(local: EstablecimientoInterface) {
        const modal = await this.modalController.create({
            component: ModalInfoLocalComponent,
            componentProps: {
                local
            }
        });
        return await modal.present();
    }

    cargarLocales() {

    }


    async inicializarMapa(latitud: number, longitud: number, device: DeviceInfo) {

        const configuracion: InicializarMapa = {
            longitud,
            latitud,
            zoom: 14,
            nombreMapa: 'mapa-locales',
            intervalo: '3000',
            // imagenPuntoUsuario: true,
            mostrarEscala: true,
            mostrarPuntoUsuario: true,
            mostrarIrAPuntoUsuario: true,
        };
        this.map = this._openlayersService.inicializarMapaOpenLayers(configuracion, device);

        const marcadores: MarcadorImagenOpenLayer[] = this.edificios.map(
            (edificio) => {
                const direccion = edificio.direccion as DireccionInterface;
                const objetoImagen = {
                    img: 'assets/imagenes/alitas-logo.png',
                    idMarcador: edificio.id,
                    configuracionTexto: {
                        nombreAMostrar: edificio.nombre.slice(0, 15).trim()
                    },
                    ...edificio
                };
                return {
                    latitud: +direccion.localizacion.localizacion.coordinates[0],
                    longitud: +direccion.localizacion.localizacion.coordinates[1],
                    objetoMarcadorImagen: objetoImagen,
                };
            });

        // const marcadores = [];

        this.map = this._openlayersService.cargarPuntosConImagenes(marcadores, this.map);

        this.map = this._openlayersService.escucharCambios(
            this.map,
            (objeto: ObjetoEventoClickOpenLayer<EdificioInterface>) => {
                if (objeto.salioDeFoco) {
                    // Se ejecuta este codigo cuando sale de foco (das click afuera de la imagen ya seleccionada)
                    // this.lubricadoraSeleccionada = undefined;
                } else {
                    // Se ejecuta este codigo cuando das click a una imagen seleccionada
                    this.abrirModalDeInformacionDeEdificio(objeto.objetoImagen.id, objeto.coordenadas);
                }
            }
        );


        this._openlayersService
            .emitioIrAPosicionActual
            .subscribe(
                async () => {
                    try {
                        const coordenadas = await Geolocation.getCurrentPosition();
                        this.map = this._openlayersService
                            .centrarEnLatitudLongitud(
                                this.map,
                                coordenadas.coords.latitude,
                                coordenadas.coords.longitude
                            );
                    } catch (e) {
                        console.error({
                            error: e,
                            mensaje: 'Error llevando al usuario a su posicion actual',
                        });
                    }
                }
            );

    }

    async abrirModalDeInformacionDeEdificio(edificioId: number, coordenadas: { latitud: number, longitud: number }) {
        this._cargandoService.habilitarCargandoService();
        try {

            const edificio = await this._edificioService.obtenerInformacionEdificio(edificioId);
            const modal = await this.modalController.create({
                component: ModalMostrarInformacion,
                componentProps: {
                    edificio,
                    coordenadas,
                }
            });
            this._cargandoService.deshabilitarCargandoService();
            await modal.present();
        } catch (error) {
            console.error({
                error,
                mensaje: 'Error buscando edificios',
                data: {
                    edificioId,
                    coordenadas,
                }
            });
            this._cargandoService.deshabilitarCargandoService();
        }

    }


}
