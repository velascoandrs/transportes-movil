import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParadasPage } from './paradas.page';

describe('ParadasPage', () => {
  let component: ParadasPage;
  let fixture: ComponentFixture<ParadasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParadasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParadasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
