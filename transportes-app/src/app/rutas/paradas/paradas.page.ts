import {Component, OnInit} from '@angular/core';
import {ParadaRestService} from '../../servicios/rest/parada.service';
import {from} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {ParadaInterface} from '../../interfaces/rest/parada.interface';

@Component({
    selector: 'app-paradas',
    templateUrl: './paradas.page.html',
    styleUrls: ['./paradas.page.scss'],
})
export class ParadasPage implements OnInit {
    constructor(
        private readonly _paradaService: ParadaRestService
    ) {
    }

    ngOnInit() {
    }

    escucharFormulario(evento) {
        console.log(evento);
    }

    buscarParada(event) {
        console.log(event);
        const consulta = {
            where: {
                nombre: [`Like("%25${event.query ? event.query : event}%25")`]
            }
        };
        return from(
            this._paradaService.findAll('criterioBusqueda=' + JSON.stringify(consulta))
        )
            .pipe(
                mergeMap(
                    (respuestaConsulta: [ParadaInterface[], number]) => {
                        return respuestaConsulta[0];
                    }
                )
            );
    }

}
