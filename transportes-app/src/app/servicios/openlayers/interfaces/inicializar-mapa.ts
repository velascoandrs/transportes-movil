export interface InicializarMapa {
  latitud: number;
  longitud: number;
  zoom?: 17 | number;
  nombreMapa: 'map' | string;
  intervalo: 300 | string;
  mostrarPuntoUsuario?: boolean;
  mostrarEscala?: boolean;
  mostrarIrAPuntoUsuario?: boolean;
  configuracionBotonIrAPuntoUsuario?: {
    html: string,
    claseCss: string,
    titulo: string,
  };
  imagenPuntoUsuario?: any;
}
