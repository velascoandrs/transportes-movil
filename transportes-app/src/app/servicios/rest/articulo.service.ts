import {environment} from '../../../environments/environment';
import {HTTP} from '@ionic-native/http/ngx';
import {IonicRestPrincipalService} from './principal-rest.service';
import {Injectable} from '@angular/core';
import {ArticuloInterface} from '../../interfaces/rest/articulo.interface';

@Injectable()
export class ArticuloRestService extends IonicRestPrincipalService<ArticuloInterface> {
    constructor(http: HTTP) {
        super(environment.url, '/articulo', http, {});
    }
}
