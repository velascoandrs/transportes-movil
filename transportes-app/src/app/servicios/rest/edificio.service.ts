import {EdificioInterface} from '../../interfaces/rest/edificio.interface';
import {environment} from '../../../environments/environment';
import {HTTP} from '@ionic-native/http/ngx';
import {IonicRestPrincipalService} from './principal-rest.service';
import {Injectable} from '@angular/core';
import {ObtenerEdificioConsulta} from '../../interfaces/rest/obtener-edificio-consulta';

@Injectable()
export class EdificioService extends IonicRestPrincipalService<EdificioInterface> {
    constructor(http: HTTP) {
        super(environment.url, '/edificio', http, {});
    }

    obtenerEdificios(criterioBusqueda: ObtenerEdificioConsulta): any {
        const url = environment.url + this.segmento + '/obtener-edificios-cercanos';
        return new Promise(
            async (res, rej) => {
                try {
                    const request = await this._http.post(
                        url,
                        criterioBusqueda,
                        undefined);
                    const respuesta = JSON.parse(request.data);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }

    obtenerInformacionEdificio(idEdificio?: number | string) {
        const url = environment.url + this.segmento + '/obtener-edificio-empresa-direccion-por-id' + `/${idEdificio}`;
        return new Promise(
            async (res, rej) => {
                try {
                    const request = await this._http.get(
                        url,
                        undefined,
                        undefined
                    );
                    const respuesta = JSON.parse(request.data);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }
}
