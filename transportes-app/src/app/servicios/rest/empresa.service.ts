import {EmpresaInterface} from '../../interfaces/rest/empresa.interface';
import {environment} from '../../../environments/environment';
import {HTTP} from '@ionic-native/http/ngx';
import {IonicRestPrincipalService} from './principal-rest.service';
import {Injectable} from '@angular/core';

@Injectable()
export class EmpresaService extends IonicRestPrincipalService<EmpresaInterface> {
    constructor(http: HTTP) {
        super(environment.url, '/empresa', http, {});
    }
}
