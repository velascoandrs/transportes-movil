import {Injectable} from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';
import {environment} from '../../../environments/environment';
import {CabecerasIonic} from '../../interfaces/rest/cabecera-ionic-interface';
import {LocalizacionInterface} from '../../interfaces/rest/localizacion.interface';

@Injectable()
export class LocalizacionService {
    url: string;
    segmento: string;
    cabeceras: CabecerasIonic;

    constructor(
        protected _http: HTTP,
    ) {
        this._http.setDataSerializer('json');
        this.url = environment.url;
        this.segmento = '/localizacion';
    }

    buscarLocalizacion(data: any = {}, cabeceras: CabecerasIonic = {}) {
        const url = this.url + this.segmento + '/buscar-localizacion';
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    const request = await this._http.get(url, {datos: JSON.stringify(data)}
                        , headers);
                    const respuesta = JSON.parse(request.data);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }

    crearLocalizacion(localizacion: { localizacion: LocalizacionInterface }) {
        const url = this.url + this.segmento;
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...this.cabeceras,
                    };
                    const request = await this._http
                        .post(
                            url,
                            localizacion,
                            headers,
                        );
                    const respuesta = JSON.parse(request.data);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }

    actualizarLocalizacion(id: string | number, localizacion: { localizacion: LocalizacionInterface }) {
        const url = this.url + this.segmento + `/${id}`;
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...this.cabeceras,
                    };
                    const request = await this._http
                        .put(
                            url,
                            localizacion,
                            headers,
                        );
                    res({
                        mensaje: 'Actualizado',
                    });
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }
}
