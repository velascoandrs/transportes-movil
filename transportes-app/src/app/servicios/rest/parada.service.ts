import {environment} from '../../../environments/environment';
import {HTTP} from '@ionic-native/http/ngx';
import {IonicRestPrincipalService} from './principal-rest.service';
import {Injectable} from '@angular/core';
import {ParadaInterface} from '../../interfaces/rest/parada.interface';

@Injectable()
export class ParadaRestService extends IonicRestPrincipalService<ParadaInterface> {
    constructor(http: HTTP) {
        super(environment.url, '/parada', http, {});
    }
}
