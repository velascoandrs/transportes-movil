import {HTTP} from '@ionic-native/http/ngx';
import {CriterioBusquedaFindWhereOr} from '../../interfaces/rest/criterio-busqueda';
import {CabecerasIonic} from '../../interfaces/rest/cabecera-ionic-interface';
import {RespuestaToken} from '../../interfaces/respuesta-token';
import {Auth0Service} from '../../submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service';
import {Observable} from 'rxjs';

export class IonicRestPrincipalService<Entidad> {
    protected url: string;
    protected segmento: string;
    cabeceras: CabecerasIonic;
    estaRefrescandoToken = false;

    constructor(
        url,
        segmento,
        // tslint:disable-next-line:variable-name
        protected _http: HTTP,
        // protected _auth0Service: Auth0Service,
        cabeceras: CabecerasIonic = {},
    ) {
        this._http.setDataSerializer('json');
        this.url = url;
        this.segmento = segmento;
        this.cabeceras = cabeceras;
        setTimeout(
            () => {

            },
            1
        );
    }

    findAll(criterioBusqueda: any = {}, cabeceras: CabecerasIonic = {}): Promise<[Entidad[], number]> {
        const url = this.url + this.segmento;
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    console.log(url);
                    const request = await this._http.get(url, {
                        criterioBusqueda: JSON.stringify(criterioBusqueda)
                    }, headers);
                    const respuesta = JSON.parse(request.data) as [Entidad[], number];
                    console.log(headers);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }

    findWhereOr(criterioBusqueda: CriterioBusquedaFindWhereOr, cabeceras: CabecerasIonic = {}): Promise<[[Entidad], number] | any> {
        const url = this.url + this.segmento + '/findWhereOr' + '?' + JSON.stringify(criterioBusqueda);
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    const request = await this._http.get(url, undefined, headers);
                    const respuesta = request.data as [[Entidad], number];
                    console.log(headers);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }

    count(criterioBusqueda: any, cabeceras: CabecerasIonic = {}): Promise<[[Entidad], number] | any> {
        const url = this.url + this.segmento + '/count' + '?' + 'criterioBusqueda=' + JSON.stringify(criterioBusqueda);
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    const request = await this._http.get(url, undefined, headers);
                    const respuesta = request.data as { registros: number };
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }

    findOne(id: string | number | Date, cabeceras: CabecerasIonic = {}): Promise<Entidad | any> {
        const url = this.url + this.segmento + `/${id}`;
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    const request = await this._http.get(url, undefined, headers);
                    const respuesta = JSON.parse(request.data) as Entidad;
                    console.log(headers);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }

    create(nuevoRegistro: Entidad, cabeceras: CabecerasIonic = {}): Promise<Entidad | any> {
        console.log(this.cabeceras);
        const url = this.url + this.segmento + '';
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    const request = await this._http.post(url, nuevoRegistro, headers);
                    const respuesta = JSON.parse(request.data) as Entidad;
                    console.log(headers);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );
    }

    delete(id: string | number | Date, cabeceras: CabecerasIonic = {}): Promise<{ mensaje: string } | any> {
        const url = this.url + this.segmento + `/${id}`;
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    const request = await this._http.delete(url, undefined, headers);
                    const respuesta = request.data as { mensaje: string };
                    console.log(headers);
                    res(respuesta);
                } catch (e) {
                    console.error(e);
                    rej(e);
                }
            }
        );

    }

    update(id: string | number | Date, datosAActualizar, cabeceras: CabecerasIonic = {}): Promise<Entidad | any> {
        const url = this.url + this.segmento + `/${id}`;
        return new Promise(
            async (res, rej) => {
                try {
                    const headers = {
                        ...cabeceras,
                        ...this.cabeceras,
                    };
                    const request = await this._http.put(url, datosAActualizar, headers);
                    const respuesta = JSON.parse(request.data) as Entidad;
                    res(respuesta);
                } catch (error) {
                    console.error({
                        error,
                        mensaje: 'Error actualizando'
                    });
                    rej({
                        error
                    });
                }
            }
        );
    }

    // async reintentarConNuevoToken(
    //     funcion: 'findAll' | 'findWhereOr' | 'count' | 'findOne' | 'create' | 'delete' | 'update',
    //     peticion: {
    //         criterioBusqueda?,
    //         id?,
    //         nuevoRegistro?,
    //         datosAActualizar?
    //     }
    // ) {
    //     try {
    //         if (this._auth0Service.estaLogeado) {
    //             await this.establecerTokenEnAuth0();
    //             this.estaRefrescandoToken = true;
    //             let respuesta;
    //             if (funcion === 'findAll') {
    //                 respuesta = await this.findAll(peticion.criterioBusqueda);
    //             }
    //             if (funcion === 'findWhereOr') {
    //                 respuesta = await this.findWhereOr(peticion.criterioBusqueda);
    //             }
    //             if (funcion === 'count') {
    //                 respuesta = await this.count(peticion.criterioBusqueda);
    //             }
    //             if (funcion === 'findOne') {
    //                 respuesta = await this.findOne(peticion.id);
    //             }
    //             if (funcion === 'create') {
    //                 respuesta = await this.create(peticion.nuevoRegistro);
    //             }
    //             if (funcion === 'update') {
    //                 respuesta = await this.update(peticion.id, peticion.datosAActualizar);
    //             }
    //             if (funcion === 'delete') {
    //                 respuesta = await this.delete(peticion.id);
    //             }
    //             this.estaRefrescandoToken = false;
    //             if (respuesta.error === 403) {
    //                 this._auth0Service.logout();
    //             } else {
    //                 return respuesta;
    //             }
    //         } else {
    //             console.error({
    //                 error: 'No esta logeado',
    //                 mensaje: 'Debemos desloggearlo'
    //             });
    //             this._auth0Service.logout();
    //         }
    //     } catch (error) {
    //         console.error({
    //             error,
    //             mensaje: 'Error en refrescar token, debemos desloggearlo'
    //         });
    //         this._auth0Service.logout();
    //     }
    // }


}












