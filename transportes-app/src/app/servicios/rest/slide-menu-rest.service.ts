import { environment } from '../../../environments/environment';
import { HTTP } from '@ionic-native/http/ngx';
import { IonicRestPrincipalService } from './principal-rest.service';
import { Injectable } from '@angular/core';
import { SlideMenuInterface } from '../../interfaces/rest/slide-menu.interface';

@Injectable()
export class SlideMenuRestService extends IonicRestPrincipalService<
  SlideMenuInterface
> {
  constructor(http: HTTP) {
    super(environment.url, '/slide-menu', http, {});
  }
}
