import { CargandoService } from './cargando-service/cargando.service';
import { ArticuloRestService } from './rest/articulo.service';
import { DireccionRestService } from './rest/direccion-rest.service';
import { EdificioService } from './rest/edificio.service';
import { EmpresaService } from './rest/empresa.service';
import { EstablecimientoRestService } from './rest/establecimiento-rest.service';
import { ToasterService } from './toaster-service/toaster.service';
import { OpenlayersService } from './openlayers/openlayers.service';
import { LocalizacionService } from './rest/localizacion.service';
import { SlideMenuRestService } from './rest/slide-menu-rest.service';
import { HorarioSlideMenuRestService } from './rest/horario-slide-menu-rest.service';
import {ParadaRestService} from './rest/parada.service';

export const SERVICIOS_PRINCIPAL = [
  CargandoService,
  ArticuloRestService,
  DireccionRestService,
  EdificioService,
  EmpresaService,
  EstablecimientoRestService,
  ToasterService,
  OpenlayersService,
  LocalizacionService,
  SlideMenuRestService,
  HorarioSlideMenuRestService,
  ParadaRestService,
];
