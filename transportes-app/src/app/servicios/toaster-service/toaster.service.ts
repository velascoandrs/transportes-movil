import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable()
export class ToasterService {

    constructor(public toastController: ToastController) {
    }

    async mostrarToast(texto: string, color: string = 'danger', tiempo: number = 2000, position: 'bottom' | 'top' | 'middle' = 'bottom') {
        const toast = await this.toastController.create({
            message: texto,
            duration: tiempo,
            color,
            position
        });
        toast.present();
    }
}
