import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {TabsPage} from './tabs.page';
import {RUTAS_TAB_AUTH} from '../submodulos/submodulo-auth0-movil/constantes/rutas-tab-auth';

const routes: Routes = [
    {
        path: '',
        component: TabsPage,
        children: [
            {
                path: 'locales-mapa',
                children: [
                    {
                        path: '',
                        loadChildren: '../rutas/locales-mapa/locales-mapa.module#LocalesMapaPageModule'
                    }
                ],
            },
            {
                path: 'paradas',
                children: [
                    {
                        path: '',
                        loadChildren: '../rutas/paradas/paradas.module#ParadasPageModule',
                    }
                ]
            },
            ...RUTAS_TAB_AUTH,
            {
                path: '',
                redirectTo: 'locales-mapa',
                pathMatch: 'full'
            }
        ]
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [TabsPage]
})
export class TabsPageModule {
}
