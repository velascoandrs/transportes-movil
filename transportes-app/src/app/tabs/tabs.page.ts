import {Component, OnInit} from '@angular/core';
import {Auth0Service} from '../submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service';
@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.page.html',
    styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

    constructor(
        public readonly _authService: Auth0Service,
    ) {
    }

    ngOnInit() {
    }

    vaACambiarATabDePedidos() {
        // this._pedidoCarritoEmitterService.cargarUltimoPedido();
    }

}
