/* tslint:disable */
// 1) Los nombres de las llaves deben de estar en comillas dobles
// 2) Los strings en las comillas deben de estar en comillas dobles
// 3) El objeto no debe de terminar con ",", se debe de tratar como
// a un JSON
export const environment = {
    "production": true,
    "habilitadoSeguridad": true,
    "versionActualAndroid": "1.0.7",
    "versionActualiOS": "1.0.7",
    "linkInstalacionAndroid": "https://play.google.com/store/apps/details?id=com.victoriacreativelabs.alitasdelcadilac&hl=en",
    "linkInstalacionIos": "https://apps.apple.com/us/app/alitas-del-cadillac/id1483995407",
    "url": "https://alitas.cadillac.manticore-labs.com",
    "OneSignalAppId": "10e62db3-0ec5-40bd-b45c-a4dc6ca23193",
    "oneSignal": {
        "appId": "10e62db3-0ec5-40bd-b45c-a4dc6ca23193",
        "googleProjectNumber": "882416910490"
    },
    "versionCodeAndroid": "3",
    "versionNameAndroid": "2.1",
    "versionVersionIOS": "2.0",
    "versionBuildIOS": "4",
    "urlGoogleCloudStorage": "https://storage.googleapis.com",
    "portGoogleCloudStorage": "/tbm-imagenes-publicas/alitas-cadillac",
    "auth0": {
        // "clientId": "971tc3Ij27pb4O0R6rkykhTOFsMoreLU",
        "clientId": "29TLH7m7DXzsvPR77jP2S5epjFWuLTAv",
        "domain": "prueba-submodulo-fear.auth0.com",
        "clientID": '29TLH7m7DXzsvPR77jP2S5epjFWuLTAv'
    }
};
