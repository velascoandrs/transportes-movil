/* tslint:disable */
// 1) Los nombres de las llaves deben de estar en comillas dobles
// 2) Los strings en las comillas deben de estar en comillas dobles
// 3) El objeto no debe de terminar con ",", se debe de tratar como
// a un JSON
export const environment = {
    "production": false,
    "habilitadoSeguridad": false,
    "versionActualAndroid": "1.0.7",
    "versionActualiOS": "1.0.7",
    "linkInstalacionAndroid": "https://play.google.com/store/apps/details?id=com.victoriacreativelabs.alitasdelcadilac&hl=en",
    "linkInstalacionIos": "https://apps.apple.com/us/app/alitas-del-cadillac/id1483995407",
    "url": "http://192.168.20.117:8080",
    "port": "8080",
    // "url": "http://192.168.100.145:8080",
    "oneSignal": {
        "appId": "10e62db3-0ec5-40bd-b45c-a4dc6ca23193",
        "googleProjectNumber": "882416910490"
    },
    // "url": "http://192.168.1.25:8080",
    // "url": "http://192.168.20.101:8080",
    // "url": "http://192.168.0.104:8080",
    "versionCodeAndroid": "20009",
    "versionNameAndroid": "2.0.6",
    "versionVersionIOS": "2.0.6",
    "versionBuildIOS": "16",
    "urlGoogleCloudStorage": "https://storage.googleapis.com",
    "portGoogleCloudStorage": "/tbm-imagenes-publicas/alitas-cadillac",
    "auth0": {
        // "clientId": "971tc3Ij27pb4O0R6rkykhTOFsMoreLU",
        "clientId": "29TLH7m7DXzsvPR77jP2S5epjFWuLTAv",
        "domain": "prueba-submodulo-fear.auth0.com",
        "clientID": '29TLH7m7DXzsvPR77jP2S5epjFWuLTAv'
    }
};
