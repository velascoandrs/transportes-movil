(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rutas-locales-mapa-locales-mapa-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/modales/mostrar-informacion/mostrar-informacion.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/modales/mostrar-informacion/mostrar-informacion.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"warning\">\n        <ion-title class=\"ion-text-center\">\n            <img class=\"icono-arriba icono-arriba-espacio-modal\" src=\"assets/imagenes/logo-alitas-cabecera.svg\" alt=\"logo\">\n        </ion-title>\n        <ion-buttons slot=\"primary\">\n            <ion-button (click)=\"dismiss()\">\n                <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\">\n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <h2>Local {{edificio.nombre}}</h2>\n                <app-mostrar-direccion\n                        [direccion]=\"edificio.direccion\"\n                >\n\n                </app-mostrar-direccion>\n                <p *ngIf=\"edificio.telefono || edificio.whatsapp\">Teléfono o Whatsapp:</p>\n                <p *ngIf=\"edificio.telefono || edificio.whatsapp\"> {{edificio.telefono}} {{edificio.whatsapp}} </p>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n<!--                <a target=\"_blank\" [href]=\"'https://www.google.com/maps/search/?api=1&query='+ coordenadas.latitud+ ','+coordenadas.longitud\">-->\n                    <ion-button expand=\"block\" color=\"success\" target=\"_blank\" [href]=\"'https://www.google.com/maps/search/?api=1&query='+ coordenadas.latitud+ ','+coordenadas.longitud\">\n                        Abrir en Google Maps\n                        <img style=\"height:48px;width:48px;\" src=\"../../../../assets/imagenes/google-maps.png\">\n                    </ion-button>\n<!--                </a>-->\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n<!--                <a target=\"_blank\" [href]=\"'https://waze.com/ul?ll=' + coordenadas.latitud+ ','+coordenadas.longitud +'&z=10'\">-->\n                    <ion-button expand=\"block\" color=\"secondary\" target=\"_blank\" [href]=\"'https://waze.com/ul?ll=' + coordenadas.latitud+ ','+coordenadas.longitud +'&z=10'\">\n                        Abrir en Waze\n                        <img style=\"height:48px;width:48px;\" src=\"../../../../assets/imagenes/waze.png\">\n                    </ion-button>\n<!--                </a>-->\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-button expand=\"block\" color=\"light\"\n                            (click)=\"dismiss()\">\n                    Cancelar\n                </ion-button>\n            </ion-col>\n        </ion-row>\n\n    </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/rutas/locales-mapa/locales-mapa.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/rutas/locales-mapa/locales-mapa.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n<!--    <ion-toolbar color=\"warning\">-->\n<!--        <ion-title class=\"ion-text-center\">-->\n<!--            <img class=\"icono-arriba\" src=\"assets/imagenes/logo-alitas-cabecera.svg\" alt=\"logo\">-->\n<!--        </ion-title>-->\n<!--    </ion-toolbar>-->\n</ion-header>\n\n<ion-content>\n    <!--  tabs -->\n\n\n    <ion-grid style=\"height:10%;\">\n        <ion-row style=\"height:100%;\">\n            <ion-col>\n                <ion-tabs>\n                    <!-- Tab bar -->\n                    <ion-tab-bar slot=\"top\" class=\"blanco\">\n                        <ion-tab-button [selected]=\"mostrarMapa\"\n                                        (click)=\"cambiarTab('mapa')\">\n                            <ion-icon name=\"map\"></ion-icon>\n                            <ion-label>Mapa</ion-label>\n                        </ion-tab-button>\n                        <ion-tab-button [selected]=\"!mostrarMapa\"\n                                        (click)=\"cambiarTab('lista')\">\n                            <ion-icon name=\"paper\"></ion-icon>\n                            <ion-label>Lista locales</ion-label>\n                        </ion-tab-button>\n                    </ion-tab-bar>\n                </ion-tabs>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n    <!--  mapa-->\n    <div class=\"contenedor-lubricadoras\" *ngIf=\"mostrarMapa\">\n        <ion-grid>\n            <ion-row>\n                <ion-col>\n                    <h3 class=\"animated fadeInUp ion-text-center\">\n                        Mapa de locales\n                    </h3>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <p>Ubica a tus <span class=\"bold\">locales preferidos</span> dentro del mapa. Tú eres el punto de color\n                    <span class=\"rosado bold\">rosado</span>.</p>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-text>\n                        <!--            <p text-justify>Busca tus talleres o lubricadoras más cercanas y accede a ellas mediante-->\n                        <!--              <img class=\"imagen-icono-text\" src=\"assets/imagenes/google-maps.svg\" alt=\"\"> <span-->\n                        <!--                      class=\"bold\"> Google Maps</span> o-->\n                        <!--              <span class=\"bold\">Waze </span> <img class=\"imagen-icono-text\"-->\n                        <!--                                                   src=\"assets/imagenes/waze.svg\">.</p>-->\n                    </ion-text>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n        <div class=\"contenedor-mapa\">\n            <div id=\"mapa-locales\" class=\"mapa\">\n                <div class=\"background\">\n                    <p>CARGANDO MAPA...</p>\n                </div>\n            </div>\n        </div>\n        <div>\n\n        </div>\n    </div>\n    <!--  lista-->\n    <div *ngIf=\"mostrarLista\" style=\"height:100%;\">\n        <ion-grid>\n            <ion-row>\n                <ion-col>\n                    <img class=\"imagen\" src=\"assets/imagenes/nuestroslocales.png\">\n                    <p class=\"parrafo-modal\">\n                        Puedes buscar rápidamente tu local favorito y\n                        <span class=\"bold\">dirigirte a él</span>, también consulta los\n                        <span class=\"bold\">horarios de atención</span>.\n                    </p>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-list>\n                        <ion-item *ngFor=\"let local of establecimientos\" class=\"todo-ancho\">\n                            <ion-card class=\"carta animated fadeInRight\" *ngIf=\"local\" class=\"todo-ancho\">\n                                <ion-card-header>\n                                    <ion-card-title><h4 text-center>{{local.nombre}}</h4></ion-card-title>\n                                </ion-card-header>\n                                <ion-card-content>\n\n                                    <app-mostrar-direccion\n                                            [direccion]=\"local.edificio.direccion\"\n                                    >\n\n                                    </app-mostrar-direccion>\n\n                                    <p>Whatsapp o teléfono: {{local.edificio.whatsapp}} {{local.edificio.telefono}}</p>\n\n                                    <ion-button expand=\"full\" (click)=\"mostrarInfoLocal(local)\">\n                                        Más información\n                                    </ion-button>\n                                </ion-card-content>\n                            </ion-card>\n                        </ion-item>\n                        <ion-item>\n                            <br>\n                            <br>\n                            <br>\n                            <ion-button *ngIf=\"hayMasLocales\" (click)=\"cargarLocales()\" expand=\"block\" class=\"boton\">\n                                Cargar más locales\n                            </ion-button>\n                        </ion-item>\n                    </ion-list>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <p class=\"ion-text-right texto-pequeno\">Versión aplicativo: {{versionAplicativo}}</p>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/constantes/paginacion/paginacion.ts":
/*!*****************************************************!*\
  !*** ./src/app/constantes/paginacion/paginacion.ts ***!
  \*****************************************************/
/*! exports provided: PAGINACION */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PAGINACION", function() { return PAGINACION; });
var PAGINACION = {
    skip: 0,
    take: 5
};


/***/ }),

/***/ "./src/app/modales/mostrar-informacion/mostrar-informacion.ts":
/*!********************************************************************!*\
  !*** ./src/app/modales/mostrar-informacion/mostrar-informacion.ts ***!
  \********************************************************************/
/*! exports provided: ModalMostrarInformacion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalMostrarInformacion", function() { return ModalMostrarInformacion; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");




var ModalMostrarInformacion = /** @class */ (function () {
    function ModalMostrarInformacion(toastController, modalController) {
        this.toastController = toastController;
        this.modalController = modalController;
        this.urlGoogle = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].urlGoogleCloudStorage + ':' + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].portGoogleCloudStorage;
    }
    ModalMostrarInformacion.prototype.dismiss = function () {
        this.modalController.dismiss(undefined);
    };
    ModalMostrarInformacion.prototype.mostrarToast = function (texto, color, tiempo) {
        if (color === void 0) { color = 'danger'; }
        if (tiempo === void 0) { tiempo = 2000; }
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: texto,
                            duration: tiempo,
                            color: color
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/, null];
                }
            });
        });
    };
    ModalMostrarInformacion.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ModalMostrarInformacion.prototype, "edificio", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ModalMostrarInformacion.prototype, "coordenadas", void 0);
    ModalMostrarInformacion = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'mostrar-informacion-modal',
            template: __webpack_require__(/*! raw-loader!./mostrar-informacion.html */ "./node_modules/raw-loader/index.js!./src/app/modales/mostrar-informacion/mostrar-informacion.html"),
        })
        // tslint:disable-next-line:component-class-suffix
        ,
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], ModalMostrarInformacion);
    return ModalMostrarInformacion;
}());



/***/ }),

/***/ "./src/app/rutas/locales-mapa/locales-mapa.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/rutas/locales-mapa/locales-mapa.module.ts ***!
  \***********************************************************/
/*! exports provided: LocalesMapaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalesMapaPageModule", function() { return LocalesMapaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _locales_mapa_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./locales-mapa.page */ "./src/app/rutas/locales-mapa/locales-mapa.page.ts");
/* harmony import */ var _modales_mostrar_informacion_mostrar_informacion__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../modales/mostrar-informacion/mostrar-informacion */ "./src/app/modales/mostrar-informacion/mostrar-informacion.ts");
/* harmony import */ var _submodulos_submodulo_auth0_movil_componentes_mostrar_direccion_mostrar_direccion_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../submodulos/submodulo-auth0-movil/componentes/mostrar-direccion/mostrar-direccion.module */ "./src/app/submodulos/submodulo-auth0-movil/componentes/mostrar-direccion/mostrar-direccion.module.ts");









var routes = [
    {
        path: '',
        component: _locales_mapa_page__WEBPACK_IMPORTED_MODULE_6__["LocalesMapaPage"]
    }
];
var LocalesMapaPageModule = /** @class */ (function () {
    function LocalesMapaPageModule() {
    }
    LocalesMapaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _submodulos_submodulo_auth0_movil_componentes_mostrar_direccion_mostrar_direccion_module__WEBPACK_IMPORTED_MODULE_8__["MostrarDireccionModule"],
            ],
            declarations: [
                _locales_mapa_page__WEBPACK_IMPORTED_MODULE_6__["LocalesMapaPage"],
                _modales_mostrar_informacion_mostrar_informacion__WEBPACK_IMPORTED_MODULE_7__["ModalMostrarInformacion"]
            ],
            entryComponents: [
                _modales_mostrar_informacion_mostrar_informacion__WEBPACK_IMPORTED_MODULE_7__["ModalMostrarInformacion"]
            ]
        })
    ], LocalesMapaPageModule);
    return LocalesMapaPageModule;
}());



/***/ }),

/***/ "./src/app/rutas/locales-mapa/locales-mapa.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/rutas/locales-mapa/locales-mapa.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".imagen {\n  height: 150px;\n  margin: auto;\n  margin-left: 25%;\n}\n\n.todo-ancho {\n  width: 100%;\n}\n\n.rosado {\n  color: #CC0061;\n}\n\n.texto-pequeno {\n  font-size: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2Rldi0wNy9EZXNrdG9wL3RyYW5zcG9ydGVzLW1vdmlsL3RyYW5zcG9ydGVzLWFwcC9zcmMvYXBwL3J1dGFzL2xvY2FsZXMtbWFwYS9sb2NhbGVzLW1hcGEucGFnZS5zY3NzIiwic3JjL2FwcC9ydXRhcy9sb2NhbGVzLW1hcGEvbG9jYWxlcy1tYXBhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURDQTtFQUNFLFdBQUE7QUNFRjs7QURBQTtFQUNFLGNBQUE7QUNHRjs7QUREQTtFQUNFLGNBQUE7QUNJRiIsImZpbGUiOiJzcmMvYXBwL3J1dGFzL2xvY2FsZXMtbWFwYS9sb2NhbGVzLW1hcGEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltYWdlbiB7XG4gIGhlaWdodDogMTUwcHg7XG4gIG1hcmdpbjphdXRvO1xuICBtYXJnaW4tbGVmdDogMjUlO1xufVxuLnRvZG8tYW5jaG97XG4gIHdpZHRoOiAxMDAlO1xufVxuLnJvc2FkbyB7XG4gIGNvbG9yOiAjQ0MwMDYxO1xufVxuLnRleHRvLXBlcXVlbm97XG4gIGZvbnQtc2l6ZTogOHB4O1xufVxuIiwiLmltYWdlbiB7XG4gIGhlaWdodDogMTUwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IDI1JTtcbn1cblxuLnRvZG8tYW5jaG8ge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnJvc2FkbyB7XG4gIGNvbG9yOiAjQ0MwMDYxO1xufVxuXG4udGV4dG8tcGVxdWVubyB7XG4gIGZvbnQtc2l6ZTogOHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/rutas/locales-mapa/locales-mapa.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/rutas/locales-mapa/locales-mapa.page.ts ***!
  \*********************************************************/
/*! exports provided: LocalesMapaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalesMapaPage", function() { return LocalesMapaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _modales_modal_info_local_modal_info_local_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modales/modal-info-local/modal-info-local.component */ "./src/app/modales/modal-info-local/modal-info-local.component.ts");
/* harmony import */ var _servicios_rest_establecimiento_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../servicios/rest/establecimiento-rest.service */ "./src/app/servicios/rest/establecimiento-rest.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../servicios/cargando-service/cargando.service */ "./src/app/servicios/cargando-service/cargando.service.ts");
/* harmony import */ var _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../servicios/toaster-service/toaster.service */ "./src/app/servicios/toaster-service/toaster.service.ts");
/* harmony import */ var _constantes_paginacion_paginacion__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../constantes/paginacion/paginacion */ "./src/app/constantes/paginacion/paginacion.ts");
/* harmony import */ var _constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../constantes/mensajes/mensaje-error */ "./src/app/constantes/mensajes/mensaje-error.ts");
/* harmony import */ var _servicios_openlayers_openlayers_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../servicios/openlayers/openlayers.service */ "./src/app/servicios/openlayers/openlayers.service.ts");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _servicios_rest_edificio_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../servicios/rest/edificio.service */ "./src/app/servicios/rest/edificio.service.ts");
/* harmony import */ var _modales_mostrar_informacion_mostrar_informacion__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../modales/mostrar-informacion/mostrar-informacion */ "./src/app/modales/mostrar-informacion/mostrar-informacion.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");














var Device = _capacitor_core__WEBPACK_IMPORTED_MODULE_10__["Plugins"].Device;
var Geolocation = _capacitor_core__WEBPACK_IMPORTED_MODULE_10__["Plugins"].Geolocation;
var LocalesMapaPage = /** @class */ (function () {
    function LocalesMapaPage(_establecimientosRestService, modalController, _cargandoService, _toastService, _openlayersService, _edificioService, platform) {
        this._establecimientosRestService = _establecimientosRestService;
        this.modalController = modalController;
        this._cargandoService = _cargandoService;
        this._toastService = _toastService;
        this._openlayersService = _openlayersService;
        this._edificioService = _edificioService;
        this.platform = platform;
        this.mostrarMapa = true;
        this.mostrarLista = false;
        this.establecimientos = [];
        this.edificios = [];
        this.skip = _constantes_paginacion_paginacion__WEBPACK_IMPORTED_MODULE_7__["PAGINACION"].skip;
        this.take = 1000;
        this.versionAplicativo = '';
    }
    LocalesMapaPage.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var esAndroid, esiOS;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                esAndroid = this.platform.is('android');
                esiOS = this.platform.is('ios');
                if (esAndroid) {
                    this.versionAplicativo = _environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"].versionActualAndroid;
                }
                if (esiOS) {
                    this.versionAplicativo = _environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"].versionActualiOS;
                }
                this.cambiarTab('lista');
                console.log(this.establecimientos);
                return [2 /*return*/];
            });
        });
    };
    LocalesMapaPage.prototype.cargarMapa = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var coordenadas, device, _a, e_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this._cargandoService.habilitarCargandoService('Cargando mapa...')];
                    case 1:
                        _b.sent();
                        _b.label = 2;
                    case 2:
                        _b.trys.push([2, 8, , 10]);
                        return [4 /*yield*/, Geolocation.getCurrentPosition()];
                    case 3:
                        coordenadas = _b.sent();
                        return [4 /*yield*/, Device.getInfo()];
                    case 4:
                        device = _b.sent();
                        _a = this;
                        return [4 /*yield*/, this._edificioService.obtenerEdificios({
                                coordenadas: [coordenadas.coords.latitude, coordenadas.coords.longitude],
                                distanciaMaxima: 10000,
                                distanciaMinima: 1,
                                entidadNombre: 'edificio',
                                mostrarEmpresa: 0,
                                tipoCoordenadas: 'Point'
                            })];
                    case 5:
                        _a.edificios = _b.sent();
                        this.edificios = this.edificios
                            .filter(function (edificio) {
                            return edificio.establecimientos.some(function (e) { return e.soloDelivery === 0; });
                        });
                        return [4 /*yield*/, this.inicializarMapa(coordenadas.coords.latitude, coordenadas.coords.longitude, device)];
                    case 6:
                        _b.sent();
                        return [4 /*yield*/, this._cargandoService.deshabilitarCargandoService()];
                    case 7:
                        _b.sent();
                        return [3 /*break*/, 10];
                    case 8:
                        e_1 = _b.sent();
                        return [4 /*yield*/, this._cargandoService.deshabilitarCargandoService()];
                    case 9:
                        _b.sent();
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    LocalesMapaPage.prototype.cambiarTab = function (tabSeleccionado) {
        if (tabSeleccionado === 'mapa' && !this.mostrarMapa) {
            this.mostrarMapa = true;
            this.mostrarLista = false;
            this.cargarMapa().then().catch();
        }
        if (tabSeleccionado === 'lista' && !this.mostrarLista) {
            this.mostrarMapa = false;
            this.mostrarLista = true;
            this.buscarLocales();
        }
    };
    LocalesMapaPage.prototype.buscarLocales = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, locales, criterioBusqueda, noHayLocales, encontroLocales, e_2;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        locales = [[], 0];
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 5, , 8]);
                        return [4 /*yield*/, this._cargandoService.habilitarCargandoService('Buscando locales...')];
                    case 2:
                        _b.sent();
                        criterioBusqueda = {
                            where: {
                                habilitado: 1,
                                soloDelivery: 0,
                            },
                            skip: this.skip * this.take,
                            take: this.take,
                            relations: [
                                'edificio',
                                'edificio.direccion',
                                'horariosEstablecimiento',
                                'horariosEstablecimiento.horario'
                            ]
                        };
                        return [4 /*yield*/, this._establecimientosRestService
                                .findAll(criterioBusqueda)];
                    case 3:
                        locales = _b.sent();
                        (_a = this.establecimientos).push.apply(_a, locales[0]);
                        noHayLocales = locales[0].length === 0;
                        encontroLocales = locales[0].length > 0;
                        return [4 /*yield*/, this._cargandoService.deshabilitarCargandoService()];
                    case 4:
                        _b.sent();
                        // if (noHayLocales) {
                        //     this._toastService.mostrarToast('No hay mas locales');
                        // }
                        if (encontroLocales) {
                            this.skip = this.skip + 1;
                        }
                        if (noHayLocales) {
                            this.hayMasLocales = false;
                        }
                        return [3 /*break*/, 8];
                    case 5:
                        e_2 = _b.sent();
                        console.log(e_2);
                        return [4 /*yield*/, this._toastService.mostrarToast(_constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_8__["MENSAJE_ERROR"])];
                    case 6:
                        _b.sent();
                        return [4 /*yield*/, this._cargandoService.deshabilitarCargandoService()];
                    case 7:
                        _b.sent();
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    LocalesMapaPage.prototype.mostrarInfoLocal = function (local) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _modales_modal_info_local_modal_info_local_component__WEBPACK_IMPORTED_MODULE_2__["ModalInfoLocalComponent"],
                            componentProps: {
                                local: local
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LocalesMapaPage.prototype.cargarLocales = function () {
    };
    LocalesMapaPage.prototype.inicializarMapa = function (latitud, longitud, device) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var configuracion, marcadores;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                configuracion = {
                    longitud: longitud,
                    latitud: latitud,
                    zoom: 14,
                    nombreMapa: 'mapa-locales',
                    intervalo: '3000',
                    // imagenPuntoUsuario: true,
                    mostrarEscala: true,
                    mostrarPuntoUsuario: true,
                    mostrarIrAPuntoUsuario: true,
                };
                this.map = this._openlayersService.inicializarMapaOpenLayers(configuracion, device);
                marcadores = this.edificios.map(function (edificio) {
                    var direccion = edificio.direccion;
                    var objetoImagen = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ img: 'assets/imagenes/alitas-logo.png', idMarcador: edificio.id, configuracionTexto: {
                            nombreAMostrar: edificio.nombre.slice(0, 15).trim()
                        } }, edificio);
                    return {
                        latitud: +direccion.localizacion.localizacion.coordinates[0],
                        longitud: +direccion.localizacion.localizacion.coordinates[1],
                        objetoMarcadorImagen: objetoImagen,
                    };
                });
                // const marcadores = [];
                this.map = this._openlayersService.cargarPuntosConImagenes(marcadores, this.map);
                this.map = this._openlayersService.escucharCambios(this.map, function (objeto) {
                    if (objeto.salioDeFoco) {
                        // Se ejecuta este codigo cuando sale de foco (das click afuera de la imagen ya seleccionada)
                        // this.lubricadoraSeleccionada = undefined;
                    }
                    else {
                        // Se ejecuta este codigo cuando das click a una imagen seleccionada
                        _this.abrirModalDeInformacionDeEdificio(objeto.objetoImagen.id, objeto.coordenadas);
                    }
                });
                this._openlayersService
                    .emitioIrAPosicionActual
                    .subscribe(function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                    var coordenadas, e_3;
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                return [4 /*yield*/, Geolocation.getCurrentPosition()];
                            case 1:
                                coordenadas = _a.sent();
                                this.map = this._openlayersService
                                    .centrarEnLatitudLongitud(this.map, coordenadas.coords.latitude, coordenadas.coords.longitude);
                                return [3 /*break*/, 3];
                            case 2:
                                e_3 = _a.sent();
                                console.error({
                                    error: e_3,
                                    mensaje: 'Error llevando al usuario a su posicion actual',
                                });
                                return [3 /*break*/, 3];
                            case 3: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    LocalesMapaPage.prototype.abrirModalDeInformacionDeEdificio = function (edificioId, coordenadas) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var edificio, modal, error_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this._cargandoService.habilitarCargandoService();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 5, , 6]);
                        return [4 /*yield*/, this._edificioService.obtenerInformacionEdificio(edificioId)];
                    case 2:
                        edificio = _a.sent();
                        return [4 /*yield*/, this.modalController.create({
                                component: _modales_mostrar_informacion_mostrar_informacion__WEBPACK_IMPORTED_MODULE_12__["ModalMostrarInformacion"],
                                componentProps: {
                                    edificio: edificio,
                                    coordenadas: coordenadas,
                                }
                            })];
                    case 3:
                        modal = _a.sent();
                        this._cargandoService.deshabilitarCargandoService();
                        return [4 /*yield*/, modal.present()];
                    case 4:
                        _a.sent();
                        return [3 /*break*/, 6];
                    case 5:
                        error_1 = _a.sent();
                        console.error({
                            error: error_1,
                            mensaje: 'Error buscando edificios',
                            data: {
                                edificioId: edificioId,
                                coordenadas: coordenadas,
                            }
                        });
                        this._cargandoService.deshabilitarCargandoService();
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    LocalesMapaPage.ctorParameters = function () { return [
        { type: _servicios_rest_establecimiento_rest_service__WEBPACK_IMPORTED_MODULE_3__["EstablecimientoRestService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
        { type: _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_5__["CargandoService"] },
        { type: _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_6__["ToasterService"] },
        { type: _servicios_openlayers_openlayers_service__WEBPACK_IMPORTED_MODULE_9__["OpenlayersService"] },
        { type: _servicios_rest_edificio_service__WEBPACK_IMPORTED_MODULE_11__["EdificioService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] }
    ]; };
    LocalesMapaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-locales-mapa',
            template: __webpack_require__(/*! raw-loader!./locales-mapa.page.html */ "./node_modules/raw-loader/index.js!./src/app/rutas/locales-mapa/locales-mapa.page.html"),
            styles: [__webpack_require__(/*! ./locales-mapa.page.scss */ "./src/app/rutas/locales-mapa/locales-mapa.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_servicios_rest_establecimiento_rest_service__WEBPACK_IMPORTED_MODULE_3__["EstablecimientoRestService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_5__["CargandoService"],
            _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_6__["ToasterService"],
            _servicios_openlayers_openlayers_service__WEBPACK_IMPORTED_MODULE_9__["OpenlayersService"],
            _servicios_rest_edificio_service__WEBPACK_IMPORTED_MODULE_11__["EdificioService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]])
    ], LocalesMapaPage);
    return LocalesMapaPage;
}());



/***/ })

}]);
//# sourceMappingURL=rutas-locales-mapa-locales-mapa-module-es5.js.map