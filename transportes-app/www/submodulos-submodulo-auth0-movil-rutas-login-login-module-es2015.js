(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["submodulos-submodulo-auth0-movil-rutas-login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.html":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.html ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"warning\">\n        <ion-title class=\"ion-text-center\">\n            <img class=\"icono-arriba icono-arriba-espacio-modal\" src=\"assets/imagenes/logo-alitas-cabecera.svg\"\n                 alt=\"logo\">\n        </ion-title>\n        <ion-buttons slot=\"primary\">\n            <ion-button (click)=\"dismiss()\">\n                <ion-icon class=\"color-negro\" slot=\"icon-only\" name=\"close\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\" color=\"warning\">\n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <h4>Opciones rápidas</h4>\n                <p class=\"parrafo-pedido\">Selecciona una de las opciones rápidas o continúa navegando por el aplicativo.</p>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"8\">\n                <div class=\"padding-popup margen-carta\">\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col size=\"4\">\n                                <img class=\"imagen-pedido\" src=\"assets/imagenes/vermenu.png\" alt=\"\">\n                            </ion-col>\n                            <ion-col size=\"8\">\n                                <p class=\"parrafo-pedido\">\n                                    Explora nuestro <span class=\"bold\">delicioso menú</span> y entérate de nuestros productos.\n                                </p>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"margen-boton\">\n                    <ion-button (click)=\"irVerMenu()\"\n                                expand=\"full\">\n                        Menú\n                    </ion-button>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col size=\"8\">\n                <div class=\"padding-popup margen-carta\">\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col size=\"4\">\n                                <img class=\"imagen-pedido\" src=\"assets/imagenes/iniciarsesionregistro.png\" alt=\"\">\n                            </ion-col>\n                            <ion-col size=\"8\">\n                                <p class=\"parrafo-pedido\">\n                                    Ingresa y realiza <span class=\"bold\">pedidos</span> y disfruta de nuestras opciones\n                                    rápidamente.\n                                </p>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </div>\n            </ion-col>\n            <ion-col size=\"4\">\n                <div class=\"margen-boton\">\n                    <ion-button (click)=\"dismiss()\"\n                                expand=\"full\">\n                        Ingresar\n                    </ion-button>\n                </div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <img class=\"imagen-logo\"  src=\"assets/imagenes/alitas-logo.png\" alt=\"\">\n                <img class=\"imagen-numerouno\"  src=\"assets/imagenes/numerouno.png\" alt=\"\">\n                <!-- <p class=\"ion-text-center\">Las alitas Nº 1 del Ecuador</p> -->\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.html":
/*!***********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.html ***!
  \***********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"warning\">\n        <ion-title class=\"ion-text-center\">\n            <img class=\"icono-arriba icono-arriba-espacio-modal\" src=\"assets/imagenes/logo-alitas-cabecera.svg\" alt=\"logo\">\n        </ion-title>\n        <ion-buttons slot=\"primary\">\n            <ion-button (click)=\"dismiss()\">\n                <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\">\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"4\">\n                <img src=\"assets/imagenes/olvide-password.svg\" style=\"max-height: 92px;\">\n            </ion-col>\n            <ion-col>\n                <h1 text-center class=\"titulo\">Recupera tu contraseña</h1>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <p class=\"ion-text-justify\">Puedes recuperar tu contraseña si tienes tu <span class=\"bold\">correo confirmado</span>.\n                    Confirma el link de activación siempre en tu correo para poder acceder a esta funcionalidad.</p>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-label class=\"form-input\">*Correo :</ion-label>\n                <ion-input\n                        placeholder=\"ejemplo@manticore-labs.com\"\n                        required\n                        type=\"email\"\n                        [email]=\"true\"\n                        name=\"email\"\n                        #email=\"ngModel\"\n                        [(ngModel)]=\"parametros.email\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-button class=\"\" expand=\"block\" color=\"primary\" [disabled]=\"email.errors\"\n                            (click)=\"restablecerPassword()\">\n                    Enviar correo y recuperar contraseña\n                    <ion-icon class=\"icono-boton\" name=\"mail-unread\"></ion-icon>\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.html":
/*!***************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.html ***!
  \***************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"warning\">\n        <ion-title class=\"ion-text-center\">\n            <img class=\"icono-arriba icono-arriba-espacio-modal\" src=\"assets/imagenes/logo-alitas-cabecera.svg\"\n                 alt=\"logo\">\n        </ion-title>\n        <ion-buttons slot=\"primary\">\n            <ion-button (click)=\"dismiss()\">\n                <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n            </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\">\n    <ion-grid>\n        <ion-row>\n            <ion-col size=\"4\">\n                <img class=\"imagen-modal\" src=\"assets/imagenes/perfildeusaurio.png\">\n            </ion-col>\n            <ion-col>\n                <h2 class=\"ion-text-center titulo-modal\">Ingresa tu información</h2>\n                <p class=\"parrafo-modal\">Ayúdanos ingresando los datos que vamos a guardar en el sistema para pedidos e\n                    información en general.</p>\n            </ion-col>\n        </ion-row>\n        <!--    nombres-->\n        <ion-row>\n            <ion-col>\n                <div class=\"espaciado\"></div>\n                <div class=\"espaciado-pequeno\"></div>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-label class=\"form-input\">\n                    *Nombres :\n                    <ion-chip color=\"danger\" class=\"chip-error\"\n                              *ngIf=\"nombres.errors && nombres.dirty && nombres.touched\"\n                              (click)=\"mostrarToast('Ingresa tus nombres de 4 a 40 caracteres.')\">\n                        <ion-label color=\"danger\">\n                            <ion-icon name=\"close\"></ion-icon>\n                        </ion-label>\n                    </ion-chip>\n                </ion-label>\n                <ion-input\n                        placeholder=\"Luis Alberto\"\n                        required\n                        minlength=\"4\"\n                        maxlength=\"40\"\n                        name=\"nombres\"\n                        [debounce]=\"3\"\n                        #nombres=\"ngModel\"\n                        [(ngModel)]=\"nuevoUsuario.nombres\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <!--    apellidos-->\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-label class=\"form-input\">\n                    *Apellidos :\n                    <ion-chip color=\"danger\" class=\"chip-error\"\n                              *ngIf=\"apellidos.errors && apellidos.dirty && apellidos.touched\"\n                              (click)=\"mostrarToast('Ingresa tus apellidos entre 4 a 40 caracteres')\">\n                        <ion-label color=\"danger\">\n                            <ion-icon name=\"close\"></ion-icon>\n                        </ion-label>\n                    </ion-chip>\n                </ion-label>\n                <ion-input\n                        placeholder=\"Lopez Valencia\"\n                        required\n                        minlength=\"4\"\n                        maxlength=\"40\"\n                        name=\"apellidos\"\n                        #apellidos=\"ngModel\"\n                        [(ngModel)]=\"nuevoUsuario.apellidos\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <!--correo-->\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-label class=\"form-input\">\n                    *Correo :\n                    <ion-chip color=\"danger\" class=\"chip-error\"\n                              *ngIf=\"email.errors && email.dirty && email.touched\"\n                              (click)=\"mostrarToast('Ingresa un correo electrónico')\">\n                        <ion-label color=\"danger\">\n                            <ion-icon name=\"close\"></ion-icon>\n                        </ion-label>\n                    </ion-chip>\n                </ion-label>\n                <ion-input\n                        placeholder=\"info@manticore-labs.com\"\n                        required\n                        [email]=\"true\"\n                        type=\"email\"\n                        inputmode=\"email\"\n                        name=\"email\"\n                        #email=\"ngModel\"\n                        [(ngModel)]=\"nuevoUsuario.email\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <!--identificacion pais / cedula-->\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-label class=\"form-input\">\n                    *Identificación :\n                    <ion-chip color=\"danger\" class=\"chip-error\"\n                              *ngIf=\"identificacionPais.errors && identificacionPais.dirty && identificacionPais.touched\"\n                              (click)=\"mostrarToast('Ingresa un identificación')\">\n                        <ion-label color=\"danger\">\n                            <ion-icon name=\"close\"></ion-icon>\n                        </ion-label>\n                    </ion-chip>\n                </ion-label>\n                <ion-input\n                        placeholder=\"1714151214\"\n                        required\n                        maxlength=\"10\"\n                        type=\"text\"\n                        inputmode=\"numeric\"\n                        name=\"identificacionPais\"\n                        #identificacionPais=\"ngModel\"\n                        [(ngModel)]=\"nuevoUsuario.identificacionPais\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <!--    direccion-->\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-label class=\"form-input\">\n                    *Dirección :\n                    <ion-chip color=\"danger\" class=\"chip-error\"\n                              *ngIf=\"direccion.errors && direccion.dirty && direccion.touched\"\n                              (click)=\"mostrarToast('Ingresa tus dirección.')\">\n                        <ion-label color=\"danger\">\n                            <ion-icon name=\"close\"></ion-icon>\n                        </ion-label>\n                    </ion-chip>\n                </ion-label>\n                <ion-input\n                        placeholder=\"La gasca\"\n                        required\n                        minlength=\"4\"\n                        maxlength=\"40\"\n                        name=\"direccion\"\n                        [debounce]=\"3\"\n                        #direccion=\"ngModel\"\n                        [(ngModel)]=\"nuevoUsuario.direccion\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <!--    celular-->\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-label class=\"form-input\">\n                    *Celular :\n                    <ion-chip color=\"danger\" class=\"chip-error\"\n                              *ngIf=\"celular.errors && celular.dirty && celular.touched\"\n                              (click)=\"mostrarToast('Ingresa solo números.')\">\n                        <ion-label color=\"danger\">\n                            <ion-icon name=\"close\"></ion-icon>\n                        </ion-label>\n                    </ion-chip>\n                </ion-label>\n                <ion-input\n                        placeholder=\"0954123578\"\n                        required\n                        maxlength=\"10\"\n                        pattern=\"[0-9]*\"\n                        inputmode=\"numeric\"\n                        name=\"celular\"\n                        [debounce]=\"3\"\n                        #celular=\"ngModel\"\n                        [(ngModel)]=\"nuevoUsuario.celular\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <!--    contraseña-->\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-label class=\"form-input\">\n                    *Contraseña :\n                    <ion-chip color=\"danger\" class=\"chip-error\"\n                              *ngIf=\"password.errors && password.dirty && password.touched\"\n                              (click)=\"mostrarToast('Ingresa tu contraseña letras y números entre 8 a 16 caracteres.', 'danger', 2000)\">\n                        <ion-label color=\"danger\">\n                            <ion-icon name=\"close\"></ion-icon>\n                        </ion-label>\n                    </ion-chip>\n                </ion-label>\n                <ion-input\n                        type=\"password\"\n                        placeholder=\"********\"\n                        required\n                        name=\"password\"\n                        minlength=\"8\"\n                        maxlength=\"16\"\n                        [debounce]=\"3\"\n                        #password=\"ngModel\"\n                        pattern=\"^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$\"\n                        [(ngModel)]=\"nuevoUsuario.password\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <!--    confirmar contraseña-->\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-label class=\"form-input\">\n                    *Confirmar contraseña :\n                    <ion-chip color=\"danger\" class=\"chip-error\"\n                              *ngIf=\"confirmarPassword.errors && confirmarPassword.dirty && confirmarPassword.touched\"\n                              (click)=\"mostrarToast('Ingresa tu contraseña entre 8 a 16 caracteres.')\">\n                        <ion-label color=\"danger\">\n                            <ion-icon name=\"close\"></ion-icon>\n                        </ion-label>\n                    </ion-chip>\n                </ion-label>\n                <ion-input\n                        [disabled]=\"(!nuevoUsuario.password)\"\n                        type=\"password\"\n                        placeholder=\"********\"\n                        required\n                        minlength=\"8\"\n                        maxlength=\"16\"\n                        [debounce]=\"50\"\n                        #confirmarPassword=\"ngModel\"\n                        [(ngModel)]=\"nuevoUsuario.confirmarPassword\"\n                ></ion-input>\n            </ion-col>\n        </ion-row>\n        <!--        terminos y condiciones-->\n        <ion-row>\n            <ion-col class=\"animated slideInUp\">\n                <ion-list>\n                    <ion-item>\n                        <ion-checkbox color=\"dark\" (click)=\"aceptoTerminosCondiciones=!aceptoTerminosCondiciones\"\n                                      slot=\"start\"></ion-checkbox>\n                        <ion-label>Aceptar términos y condiciones</ion-label>\n                    </ion-item>\n                </ion-list>\n                <a class=\"ion-text-center\" href=\"http://www.alitasdelcadillac.com/wp-content/uploads/2016/09/terminos_condiciones_app_2020.pdf\">Términos y condiciones</a>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <div class=\"espaciado-pequeno\"></div>\n                <div class=\"espaciado\"></div>\n            </ion-col>\n        </ion-row>\n        <!--        boton-->\n        <ion-row>\n            <ion-col>\n                <ion-button class=\"animated slideInUp\" expand=\"block\" color=\"primary\" [disabled]=\"\n                email.errors || nombres.errors ||\n                apellidos.errors || identificacionPais.errors ||\n                direccion.errors || celular.errors || password.errors || !aceptoTerminosCondiciones\"\n                            (click)=\"registrarUsuario()\">\n                    Registrarse\n                    <ion-icon class=\"icono-boton\" name=\"mail-unread\"></ion-icon>\n                </ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n<!--    <ion-toolbar color=\"warning\">-->\n<!--        <ion-title class=\"ion-text-center\">-->\n<!--            <img class=\"icono-arriba\" src=\"assets/imagenes/logo-alitas-cabecera.svg\" alt=\"logo\">-->\n<!--        </ion-title>-->\n<!--    </ion-toolbar>-->\n</ion-header>\n\n<ion-content>\n    <div class=\"oscuro-transporte-backgroudn ocupar-todo\">\n        <ion-grid class=\"espacio-titulo\">\n            <ion-row>\n                <ion-col>\n                    <div class=\"d-flex justify-content-center\">\n                        <div class=\"p-2\">\n                            <img src=\"assets/imagenes/logo-fake.svg\" alt=\"\">\n                            <h1 class=\"titulo-bienvenido\">\n                                BIENVENIDO\n                            </h1>\n                            <p class=\"ion-text-center texto-blaco\">\n                                Ingresa tus credenciales.\n                            </p>\n                        </div>\n                    </div>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n            </ion-row>\n            <ion-row>\n                <ion-col size=\"1\"></ion-col>\n                <ion-col>\n                    <ion-grid>\n                        <ion-row>\n                            <ion-col>\n                                <ion-label class=\"form-input\">\n                                    *Correo :\n                                    <ion-chip color=\"danger\" class=\"chip-error\"\n                                              *ngIf=\"username.errors && username.dirty && username.touched\"\n                                              (click)=\"mostrarToast('Ingresa un correo electrónico.')\">\n                                        <ion-label color=\"danger\">\n                                            <ion-icon name=\"close\"></ion-icon>\n                                        </ion-label>\n                                    </ion-chip>\n                                </ion-label>\n                                <ion-input\n                                        placeholder=\"info@manticore-labs.com\"\n                                        required\n                                        [email]=\"true\"\n                                        inputmode=\"email\"\n                                        name=\"username\"\n                                        #username=\"ngModel\"\n                                        [(ngModel)]=\"usuario.username\"\n                                ></ion-input>\n\n                            </ion-col>\n                        </ion-row>\n                        <ion-row>\n                            <ion-col>\n                                <ion-label class=\"form-input\">*Contraseña :</ion-label>\n                                <ion-input\n                                        placeholder=\"XXXXXXXXXX\"\n                                        required\n                                        [disabled]=\"!usuario.username\"\n                                        type=\"password\"\n                                        name=\"password\"\n                                        #password=\"ngModel\"\n                                        [(ngModel)]=\"usuario.password\"\n                                ></ion-input>\n                            </ion-col>\n                        </ion-row>\n                    </ion-grid>\n                </ion-col>\n                <ion-col size=\"1\"></ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-button expand=\"block\"\n                                color=\"secondary\"\n                                (click)=\"login()\"\n                                [disabled]=\"password.errors || username.errors || desactivarBotonLogin\"\n                    >\n                        Ingresar\n                    </ion-button>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-button\n                            color=\"medium\"\n                            expand=\"block\"\n                            (click)=\"mostrarModalRecupearPassword()\"\n                    >\n                            Recupera tu contraseña\n                    </ion-button>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <div class=\"separador\">\n                    </div>\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-button expand=\"block\"\n                                color=\"tertiary\"\n                                (click)=\"mostrarModalRegistrarse()\"\n                    >\n                        Registrate\n                    </ion-button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n    </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/constantes/loading/configuracion-loading.ts":
/*!*************************************************************!*\
  !*** ./src/app/constantes/loading/configuracion-loading.ts ***!
  \*************************************************************/
/*! exports provided: CONFIGURACION_LOADING */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CONFIGURACION_LOADING", function() { return CONFIGURACION_LOADING; });
const CONFIGURACION_LOADING = {
    spinner: 'dots',
    duration: 5000,
    message: 'Cargando...',
    translucent: true,
    cssClass: 'custom-class custom-loading'
};


/***/ }),

/***/ "./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":root .ion-color-warning {\n  --ion-color-contrast: black !important;\n}\n\n.ion-color-warning {\n  --ion-color-contrast: black !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2Rldi0wNy9EZXNrdG9wL3RyYW5zcG9ydGVzLW1vdmlsL3RyYW5zcG9ydGVzLWFwcC9zcmMvYXBwL21vZGFsZXMvbW9kYWwtbW9zdHJhci1vcGNpb25lcy9tb2RhbC1tb3N0cmFyLW9wY2lvbmVzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9tb2RhbGVzL21vZGFsLW1vc3RyYXItb3BjaW9uZXMvbW9kYWwtbW9zdHJhci1vcGNpb25lcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLHNDQUFBO0FDQUo7O0FESUE7RUFDRSxzQ0FBQTtBQ0RGIiwiZmlsZSI6InNyYy9hcHAvbW9kYWxlcy9tb2RhbC1tb3N0cmFyLW9wY2lvbmVzL21vZGFsLW1vc3RyYXItb3BjaW9uZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6cm9vdHtcbiAgLmlvbi1jb2xvci13YXJuaW5nIHtcbiAgICAtLWlvbi1jb2xvci1jb250cmFzdDogYmxhY2sgIWltcG9ydGFudDtcbiAgfVxuXG59XG4uaW9uLWNvbG9yLXdhcm5pbmcge1xuICAtLWlvbi1jb2xvci1jb250cmFzdDogYmxhY2sgIWltcG9ydGFudDtcbn1cbiIsIjpyb290IC5pb24tY29sb3Itd2FybmluZyB7XG4gIC0taW9uLWNvbG9yLWNvbnRyYXN0OiBibGFjayAhaW1wb3J0YW50O1xufVxuXG4uaW9uLWNvbG9yLXdhcm5pbmcge1xuICAtLWlvbi1jb2xvci1jb250cmFzdDogYmxhY2sgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.ts ***!
  \************************************************************************************/
/*! exports provided: ModalMostrarOpcionesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalMostrarOpcionesComponent", function() { return ModalMostrarOpcionesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let ModalMostrarOpcionesComponent = class ModalMostrarOpcionesComponent {
    constructor(modalController) {
        this.modalController = modalController;
    }
    ngOnInit() { }
    dismiss() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.modalController.dismiss(undefined);
        });
    }
    irVerMenu() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.modalController.dismiss('seccion-menu');
        });
    }
};
ModalMostrarOpcionesComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
ModalMostrarOpcionesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-mostrar-opciones',
        template: __webpack_require__(/*! raw-loader!./modal-mostrar-opciones.component.html */ "./node_modules/raw-loader/index.js!./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.html"),
        styles: [__webpack_require__(/*! ./modal-mostrar-opciones.component.scss */ "./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
], ModalMostrarOpcionesComponent);



/***/ }),

/***/ "./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.module.ts ***!
  \*********************************************************************************/
/*! exports provided: ModalMostrarOpcionesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalMostrarOpcionesModule", function() { return ModalMostrarOpcionesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _modal_mostrar_opciones_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal-mostrar-opciones.component */ "./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





let ModalMostrarOpcionesModule = class ModalMostrarOpcionesModule {
};
ModalMostrarOpcionesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _modal_mostrar_opciones_component__WEBPACK_IMPORTED_MODULE_3__["ModalMostrarOpcionesComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"]
        ],
        entryComponents: [
            _modal_mostrar_opciones_component__WEBPACK_IMPORTED_MODULE_3__["ModalMostrarOpcionesComponent"]
        ]
    })
], ModalMostrarOpcionesModule);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.scss":
/*!*********************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.scss ***!
  \*********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1Ym1vZHVsb3Mvc3VibW9kdWxvLWF1dGgwLW1vdmlsL21vZGFsZXMvbW9kYWwtb2x2aWRvLXBhc3N3b3JkL21vZGFsLW9sdmlkby1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.ts":
/*!*******************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.ts ***!
  \*******************************************************************************************************************/
/*! exports provided: ModalOlvidoPasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalOlvidoPasswordComponent", function() { return ModalOlvidoPasswordComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../servicios/auth0/auth0.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service.ts");
/* harmony import */ var _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../servicios/cargando-service/cargando.service */ "./src/app/servicios/cargando-service/cargando.service.ts");





let ModalOlvidoPasswordComponent = class ModalOlvidoPasswordComponent {
    constructor(toastController, modalController, _auth0Service, _cargandoService) {
        this.toastController = toastController;
        this.modalController = modalController;
        this._auth0Service = _auth0Service;
        this._cargandoService = _cargandoService;
        this.parametros = {
            email: undefined
        };
    }
    dismiss() {
        this.modalController.dismiss(undefined);
    }
    mostrarToast(texto, color = 'danger', tiempo = 2000) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: texto,
                duration: tiempo,
                color
            });
            toast.present();
        });
    }
    restablecerPassword() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this._cargandoService.habilitarCargandoService('Cambiando credenciales...');
            try {
                const request = yield this._auth0Service.recupearaPassword(this.parametros.email);
                const respuesta = JSON.parse(request.data);
                this.mostrarToast(respuesta.mensaje, 'success', 4500);
                yield this._cargandoService.deshabilitarCargandoService();
                this.dismiss();
            }
            catch (e) {
                console.error(e);
                yield this._cargandoService.deshabilitarCargandoService();
                this.mostrarToast('Error, inténtalo mas tarde.');
            }
        });
    }
};
ModalOlvidoPasswordComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_3__["Auth0Service"] },
    { type: _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_4__["CargandoService"] }
];
ModalOlvidoPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-olvido-password',
        template: __webpack_require__(/*! raw-loader!./modal-olvido-password.component.html */ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.html"),
        styles: [__webpack_require__(/*! ./modal-olvido-password.component.scss */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_3__["Auth0Service"],
        _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_4__["CargandoService"]])
], ModalOlvidoPasswordComponent);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.module.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.module.ts ***!
  \****************************************************************************************************************/
/*! exports provided: ModalOlvidoPasswordModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalOlvidoPasswordModule", function() { return ModalOlvidoPasswordModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _modal_olvido_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-olvido-password.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _servicios_servicios_auth0_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../servicios/servicios-auth0.module */ "./src/app/submodulos/submodulo-auth0-movil/servicios/servicios-auth0.module.ts");






let ModalOlvidoPasswordModule = class ModalOlvidoPasswordModule {
};
ModalOlvidoPasswordModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _modal_olvido_password_component__WEBPACK_IMPORTED_MODULE_2__["ModalOlvidoPasswordComponent"],
        ],
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _servicios_servicios_auth0_module__WEBPACK_IMPORTED_MODULE_5__["ServiciosAuth0Module"]
        ],
        entryComponents: [
            _modal_olvido_password_component__WEBPACK_IMPORTED_MODULE_2__["ModalOlvidoPasswordComponent"],
        ]
    })
], ModalOlvidoPasswordModule);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.scss":
/*!*************************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.scss ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1Ym1vZHVsb3Mvc3VibW9kdWxvLWF1dGgwLW1vdmlsL21vZGFsZXMvbW9kYWwtcmVnaXN0cmFyc2UvbW9kYWwtcmVnaXN0cmFyc2UuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: ModalRegistrarseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalRegistrarseComponent", function() { return ModalRegistrarseComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../servicios/auth0/auth0.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../servicios/cargando-service/cargando.service */ "./src/app/servicios/cargando-service/cargando.service.ts");






let ModalRegistrarseComponent = class ModalRegistrarseComponent {
    constructor(toastController, modalController, _formBuilder, _auth0Service, _cargandoService) {
        this.toastController = toastController;
        this.modalController = modalController;
        this._formBuilder = _formBuilder;
        this._auth0Service = _auth0Service;
        this._cargandoService = _cargandoService;
        this.nuevoUsuario = {};
        // tslint:disable-next-line:max-line-length
        this.patron = '/^[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i';
        this.aceptoTerminosCondiciones = false;
        this.patronRegex = new RegExp(this.patron);
    }
    dismiss() {
        this.modalController.dismiss(undefined);
    }
    ngOnInit() {
    }
    mostrarToast(texto, color = 'danger', tiempo = 2000) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: texto,
                duration: tiempo,
                color
            });
            toast.present();
        });
    }
    registrarUsuario() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this._cargandoService.habilitarCargandoService('Creando cuenta...');
            const coincidirContraseias = this.nuevoUsuario.password === this.nuevoUsuario.confirmarPassword;
            console.log({ coincidirContraseias });
            if (coincidirContraseias) {
                try {
                    const usuarioCreado = yield this._auth0Service.guardarUsuario(this.nuevoUsuario);
                    yield this._cargandoService.deshabilitarCargandoService();
                    yield this.mostrarToast('Ingresa tus credenciales para acceder al aplicativo', 'success');
                    yield this.modalController.dismiss({
                        username: this.nuevoUsuario.email,
                        password: this.nuevoUsuario.password,
                    });
                }
                catch (e) {
                    console.log({
                        error: e,
                        mensaje: 'Error creando usuario'
                    });
                    yield this._cargandoService.deshabilitarCargandoService();
                    let mensaje = '';
                    if (e.status) {
                        if (e.status === 400) {
                            mensaje = 'Error, correo o cédula ya existen.';
                        }
                        else {
                            mensaje = 'Ocurrió un error, inténtalo de nuevo';
                        }
                    }
                    else {
                        mensaje = 'Ocurrió un error, inténtalo de nuevo';
                    }
                    this.mostrarToast(mensaje);
                }
            }
            else {
                yield this._cargandoService.deshabilitarCargandoService();
                this.mostrarToast('Las contraseñas no coinciden, intentalo de nuevo');
            }
        });
    }
};
ModalRegistrarseComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__["Auth0Service"] },
    { type: _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_5__["CargandoService"] }
];
ModalRegistrarseComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-modal-registrarse',
        template: __webpack_require__(/*! raw-loader!./modal-registrarse.component.html */ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.html"),
        styles: [__webpack_require__(/*! ./modal-registrarse.component.scss */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__["Auth0Service"],
        _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_5__["CargandoService"]])
], ModalRegistrarseComponent);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.module.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.module.ts ***!
  \********************************************************************************************************/
/*! exports provided: ModalRegistrarseModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalRegistrarseModule", function() { return ModalRegistrarseModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _modal_registrarse_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-registrarse.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");






let ModalRegistrarseModule = class ModalRegistrarseModule {
};
ModalRegistrarseModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _modal_registrarse_component__WEBPACK_IMPORTED_MODULE_2__["ModalRegistrarseComponent"],
        ],
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_5__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
        ],
        entryComponents: [
            _modal_registrarse_component__WEBPACK_IMPORTED_MODULE_2__["ModalRegistrarseComponent"],
        ]
    })
], ModalRegistrarseModule);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.module.ts ***!
  \******************************************************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.ts");
/* harmony import */ var _modales_modal_registrarse_modal_registrarse_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../modales/modal-registrarse/modal-registrarse.module */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.module.ts");
/* harmony import */ var _servicios_servicios_auth0_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../servicios/servicios-auth0.module */ "./src/app/submodulos/submodulo-auth0-movil/servicios/servicios-auth0.module.ts");
/* harmony import */ var _modales_modal_olvido_password_modal_olvido_password_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modales/modal-olvido-password/modal-olvido-password.module */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.module.ts");
/* harmony import */ var _modales_modal_cambiar_correo_modal_cambiar_correo_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../modales/modal-cambiar-correo/modal-cambiar-correo.module */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-cambiar-correo/modal-cambiar-correo.module.ts");
/* harmony import */ var _modales_modal_mostrar_opciones_modal_mostrar_opciones_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../modales/modal-mostrar-opciones/modal-mostrar-opciones.module */ "./src/app/modales/modal-mostrar-opciones/modal-mostrar-opciones.module.ts");












const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _modales_modal_registrarse_modal_registrarse_module__WEBPACK_IMPORTED_MODULE_7__["ModalRegistrarseModule"],
            _servicios_servicios_auth0_module__WEBPACK_IMPORTED_MODULE_8__["ServiciosAuth0Module"],
            _modales_modal_olvido_password_modal_olvido_password_module__WEBPACK_IMPORTED_MODULE_9__["ModalOlvidoPasswordModule"],
            _modales_modal_cambiar_correo_modal_cambiar_correo_module__WEBPACK_IMPORTED_MODULE_10__["ModalCambiarCorreoModule"],
            _modales_modal_mostrar_opciones_modal_mostrar_opciones_module__WEBPACK_IMPORTED_MODULE_11__["ModalMostrarOpcionesModule"],
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".imagen-logo {\n  max-height: 135px;\n  margin-left: 27%;\n}\n\n.titulo-bienvenido {\n  border-bottom: 1px solid #f5efff;\n  font-size: 2rem;\n  color: #f5efff;\n  margin-top: 2rem;\n}\n\nion-input {\n  background-color: white;\n  border-radius: 13px;\n}\n\n.espacio-al-final {\n  background-color: #f7e400;\n}\n\n.imagen-carrito-cadillac {\n  margin: auto;\n  margin-left: 30%;\n  height: 8rem;\n  -webkit-transform: translate(0px, 80px);\n          transform: translate(0px, 80px);\n}\n\n.triangulo {\n  width: 0;\n  height: 0;\n  border-bottom: 95px solid #0bb8cc;\n  border-left: 425px solid transparent;\n}\n\n.botones {\n  -webkit-transform: translate(0px, 55px);\n          transform: translate(0px, 55px);\n}\n\n.mat-form-field-appearance-outline .mat-form-field-outline {\n  display: -webkit-box;\n  display: flex;\n  position: absolute;\n  top: 0.8em;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  pointer-events: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2Rldi0wNy9EZXNrdG9wL3RyYW5zcG9ydGVzLW1vdmlsL3RyYW5zcG9ydGVzLWFwcC9zcmMvYXBwL3N1Ym1vZHVsb3Mvc3VibW9kdWxvLWF1dGgwLW1vdmlsL3J1dGFzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc3VibW9kdWxvcy9zdWJtb2R1bG8tYXV0aDAtbW92aWwvcnV0YXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsZ0NBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUE7RUFDRSx1QkFBQTtFQUNBLG1CQUFBO0FDQ0Y7O0FERUE7RUFDRSx5QkFBQTtBQ0NGOztBREVBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLHVDQUFBO1VBQUEsK0JBQUE7QUNDRjs7QURFQTtFQUNFLFFBQUE7RUFDQSxTQUFBO0VBQ0EsaUNBQUE7RUFDQSxvQ0FBQTtBQ0NGOztBRENBO0VBQ0UsdUNBQUE7VUFBQSwrQkFBQTtBQ0VGOztBREVBO0VBQ0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0Esb0JBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL3N1Ym1vZHVsb3Mvc3VibW9kdWxvLWF1dGgwLW1vdmlsL3J1dGFzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWFnZW4tbG9nbyB7XG4gIG1heC1oZWlnaHQ6IDEzNXB4O1xuICBtYXJnaW4tbGVmdDogMjclO1xufVxuXG4udGl0dWxvLWJpZW52ZW5pZG8ge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Y1ZWZmZjtcbiAgZm9udC1zaXplOiAycmVtO1xuICBjb2xvcjogI2Y1ZWZmZjtcbiAgbWFyZ2luLXRvcDogMnJlbTtcbn1cblxuaW9uLWlucHV0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEzcHg7XG59XG5cbi5lc3BhY2lvLWFsLWZpbmFsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZTQwMDtcbn1cblxuLmltYWdlbi1jYXJyaXRvLWNhZGlsbGFjIHtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tbGVmdDogMzAlO1xuICBoZWlnaHQ6IDhyZW07XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgODBweCk7XG59XG5cbi50cmlhbmd1bG8ge1xuICB3aWR0aDogMDtcbiAgaGVpZ2h0OiAwO1xuICBib3JkZXItYm90dG9tOiA5NXB4IHNvbGlkICMwYmI4Y2M7XG4gIGJvcmRlci1sZWZ0OiA0MjVweCBzb2xpZCB0cmFuc3BhcmVudDtcbn1cbi5ib3RvbmVze1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDU1cHgpO1xufVxuXG5cbi5tYXQtZm9ybS1maWVsZC1hcHBlYXJhbmNlLW91dGxpbmUgLm1hdC1mb3JtLWZpZWxkLW91dGxpbmUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogLjgwZW07XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBib3R0b206IDA7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuIiwiLmltYWdlbi1sb2dvIHtcbiAgbWF4LWhlaWdodDogMTM1cHg7XG4gIG1hcmdpbi1sZWZ0OiAyNyU7XG59XG5cbi50aXR1bG8tYmllbnZlbmlkbyB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZjVlZmZmO1xuICBmb250LXNpemU6IDJyZW07XG4gIGNvbG9yOiAjZjVlZmZmO1xuICBtYXJnaW4tdG9wOiAycmVtO1xufVxuXG5pb24taW5wdXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTNweDtcbn1cblxuLmVzcGFjaW8tYWwtZmluYWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdlNDAwO1xufVxuXG4uaW1hZ2VuLWNhcnJpdG8tY2FkaWxsYWMge1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi1sZWZ0OiAzMCU7XG4gIGhlaWdodDogOHJlbTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMHB4LCA4MHB4KTtcbn1cblxuLnRyaWFuZ3VsbyB7XG4gIHdpZHRoOiAwO1xuICBoZWlnaHQ6IDA7XG4gIGJvcmRlci1ib3R0b206IDk1cHggc29saWQgIzBiYjhjYztcbiAgYm9yZGVyLWxlZnQ6IDQyNXB4IHNvbGlkIHRyYW5zcGFyZW50O1xufVxuXG4uYm90b25lcyB7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDBweCwgNTVweCk7XG59XG5cbi5tYXQtZm9ybS1maWVsZC1hcHBlYXJhbmNlLW91dGxpbmUgLm1hdC1mb3JtLWZpZWxkLW91dGxpbmUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMC44ZW07XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBib3R0b206IDA7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.ts":
/*!****************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.ts ***!
  \****************************************************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _modales_modal_olvido_password_modal_olvido_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../modales/modal-olvido-password/modal-olvido-password.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-olvido-password/modal-olvido-password.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../constantes/mensajes/mensaje-error */ "./src/app/constantes/mensajes/mensaje-error.ts");
/* harmony import */ var _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../servicios/auth0/auth0.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _servicios_local_storage_local_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../servicios/local-storage/local-storage.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/local-storage/local-storage.service.ts");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _constantes_loading_configuracion_loading__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../constantes/loading/configuracion-loading */ "./src/app/constantes/loading/configuracion-loading.ts");
/* harmony import */ var _servicios_rest_dispositivo_movil_usuario_rest_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../servicios/rest/dispositivo-movil-usuario-rest.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/rest/dispositivo-movil-usuario-rest.service.ts");
/* harmony import */ var _servicios_rest_dispositivo_movil_rest_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../servicios/rest/dispositivo-movil-rest.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/rest/dispositivo-movil-rest.service.ts");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "./node_modules/@ionic-native/onesignal/ngx/index.js");
/* harmony import */ var _modales_modal_registrarse_modal_registrarse_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../modales/modal-registrarse/modal-registrarse.component */ "./src/app/submodulos/submodulo-auth0-movil/modales/modal-registrarse/modal-registrarse.component.ts");
/* harmony import */ var _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../../servicios/cargando-service/cargando.service */ "./src/app/servicios/cargando-service/cargando.service.ts");
/* harmony import */ var _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../../servicios/toaster-service/toaster.service */ "./src/app/servicios/toaster-service/toaster.service.ts");
















const { Geolocation } = _capacitor_core__WEBPACK_IMPORTED_MODULE_8__["Plugins"];
let LoginPage = class LoginPage {
    constructor(toastController, modalController, loadingController, _auth0Service, _router, _localStorageService, _oneSignal, _dispositivoMovilRestService, _dispMovilUsuarioRestService, _cargandoService, _toastService) {
        this.toastController = toastController;
        this.modalController = modalController;
        this.loadingController = loadingController;
        this._auth0Service = _auth0Service;
        this._router = _router;
        this._localStorageService = _localStorageService;
        this._oneSignal = _oneSignal;
        this._dispositivoMovilRestService = _dispositivoMovilRestService;
        this._dispMovilUsuarioRestService = _dispMovilUsuarioRestService;
        this._cargandoService = _cargandoService;
        this._toastService = _toastService;
        this.configuracionLoading = _constantes_loading_configuracion_loading__WEBPACK_IMPORTED_MODULE_9__["CONFIGURACION_LOADING"];
        this.contadorIntentos = 0;
        this.desactivarBotonLogin = false;
        this.usuario = {
            username: undefined,
            password: undefined,
        };
        this.totalOpciones = 4;
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (!this._auth0Service.estaLogeado) {
            }
            // this.opcionesMenuDetalleFiltrados = this.opcionesMenuDetalle;
        });
    }
    cambioCantidad(tipo) {
        if (tipo === 'Positivo') {
            // Quitar una opcion porque ya selecciono una
            // this.totalOpciones--;
        }
        if (tipo === 'Negativo') {
            // Aumentar una opcion porque elimino una
            // this.totalOpciones++;
        }
    }
    filtrarArreglo() {
        return [];
        // .filter((oMD) => (this.totalOpciones === 0 && oMD.seleccionados > 0) || this.totalOpciones > 0);
    }
    mostrarModalRecupearPassword() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modales_modal_olvido_password_modal_olvido_password_component__WEBPACK_IMPORTED_MODULE_2__["ModalOlvidoPasswordComponent"]
            });
            return yield modal.present();
        });
    }
    login() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this._cargandoService.habilitarCargandoService('Verificando credenciales...');
            try {
                const respuesta = yield this._auth0Service.login(this.usuario.username, this.usuario.password);
                this._auth0Service.estaLogeado = respuesta.respuesta;
                this._localStorageService.setItem('estaLogeado', this._auth0Service.estaLogeado);
                this._auth0Service.usuario = respuesta.usuario;
                this._localStorageService.setItem('usuario', this._auth0Service.usuario);
                const tokenId = respuesta.respuesta.token.id_token;
                this._auth0Service.establecerToken(tokenId);
                console.log(this._auth0Service.estaLogeado, this._auth0Service.usuario);
                this.escucharPushNotifications();
                yield this._cargandoService.deshabilitarCargandoService();
                this._toastService.mostrarToast('Credenciales correctas', 'success', 1000, 'top');
                const url = ['/tabs', 'pedidos'];
                this._router.navigate(url);
            }
            catch (error) {
                this.contadorIntentos = this.contadorIntentos + 1;
                console.error({
                    error,
                    mensaje: 'Error al momento de hacer login'
                });
                this._toastService.mostrarToast(_constantes_mensajes_mensaje_error__WEBPACK_IMPORTED_MODULE_4__["MENSAJE_ERROR"] + 'O verifica tus credenciales.', 'danger', 4000);
                yield this._cargandoService.deshabilitarCargandoService();
                if (this.contadorIntentos === 5) {
                    this.desactivarBotonLogin = true;
                    setTimeout(() => {
                        this.desactivarBotonLogin = false;
                        this.contadorIntentos = 0;
                    }, 60000);
                }
            }
        });
    }
    escucharPushNotifications() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            try {
                const respuesta = yield this._oneSignal.getPermissionSubscriptionState();
                console.log('one-signal' + JSON.stringify(respuesta));
                const consultaId = {
                    where: {
                        playerID: respuesta.subscriptionStatus.userId
                    }
                };
                const idDispositivo = yield this._dispositivoMovilRestService.findAll(consultaId);
                console.log('idDispositivo[0]', idDispositivo[0]);
                if (idDispositivo[0].length > 0) {
                    const dispMovilUsuario = {
                        dispositivoMovil: idDispositivo[0][0].id,
                        datosUsuario: this._auth0Service.usuario.id,
                        habilitado: 1,
                    };
                    const existeDispMovilUsuario = yield this._dispMovilUsuarioRestService.findAll(dispMovilUsuario);
                    console.log('existe', existeDispMovilUsuario);
                    if (existeDispMovilUsuario[1] === 0) {
                        console.log('dispMovilUsuario', dispMovilUsuario);
                        const crearDispMovilUsuario = yield this._dispMovilUsuarioRestService.create(dispMovilUsuario);
                        console.log('crearDispMovilUsuario', crearDispMovilUsuario);
                    }
                }
                else {
                    if (respuesta) {
                        const dispositivo = {
                            playerID: respuesta.subscriptionStatus.userId,
                            habilitado: 1,
                        };
                        try {
                            const dispositivoCreado = yield this._dispositivoMovilRestService.create(dispositivo);
                            const dispMovilUsuarioACrearse = {
                                dispositivoMovil: dispositivoCreado.id,
                                datosUsuario: this._auth0Service.usuario.id,
                                habilitado: 1,
                            };
                            const crearDispMovilUsuario = yield this._dispMovilUsuarioRestService.create(dispMovilUsuarioACrearse);
                            console.log('Se registro el dispositivo movil correctamente', crearDispMovilUsuario);
                        }
                        catch (error) {
                            console.error({
                                error,
                                mensaje: 'Puede que ya este creado'
                            });
                        }
                    }
                }
            }
            catch (error) {
                console.error({
                    error,
                    mensaje: 'Error al momento de crear dispositivos moviles'
                });
            }
        });
    }
    mostrarModalRegistrarse() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modales_modal_registrarse_modal_registrarse_component__WEBPACK_IMPORTED_MODULE_13__["ModalRegistrarseComponent"]
            });
            yield modal.present();
            const resultadoModal = yield modal.onDidDismiss();
            if (resultadoModal.data) {
                this.usuario.username = resultadoModal.data.username;
                this.usuario.password = resultadoModal.data.password;
                this.login();
            }
        });
    }
    mostrarToast(mensaje) {
        this._toastService.mostrarToast(mensaje);
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_5__["Auth0Service"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _servicios_local_storage_local_storage_service__WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"] },
    { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_12__["OneSignal"] },
    { type: _servicios_rest_dispositivo_movil_rest_service__WEBPACK_IMPORTED_MODULE_11__["DispositivoMovilRestService"] },
    { type: _servicios_rest_dispositivo_movil_usuario_rest_service__WEBPACK_IMPORTED_MODULE_10__["DispositivoMovilUsuarioRestService"] },
    { type: _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_14__["CargandoService"] },
    { type: _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_15__["ToasterService"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.html"),
        styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/submodulos/submodulo-auth0-movil/rutas/login/login.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_5__["Auth0Service"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
        _servicios_local_storage_local_storage_service__WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"],
        _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_12__["OneSignal"],
        _servicios_rest_dispositivo_movil_rest_service__WEBPACK_IMPORTED_MODULE_11__["DispositivoMovilRestService"],
        _servicios_rest_dispositivo_movil_usuario_rest_service__WEBPACK_IMPORTED_MODULE_10__["DispositivoMovilUsuarioRestService"],
        _servicios_cargando_service_cargando_service__WEBPACK_IMPORTED_MODULE_14__["CargandoService"],
        _servicios_toaster_service_toaster_service__WEBPACK_IMPORTED_MODULE_15__["ToasterService"]])
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=submodulos-submodulo-auth0-movil-rutas-login-login-module-es2015.js.map