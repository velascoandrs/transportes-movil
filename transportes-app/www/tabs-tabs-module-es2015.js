(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tabs-tabs-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tabs/tabs.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tabs/tabs.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<ion-header>-->\n<!--  <ion-toolbar>-->\n<!--    <ion-title>tabs</ion-title>-->\n<!--  </ion-toolbar>-->\n<!--</ion-header>-->\n\n<!--<ion-content>-->\n\n<!--</ion-content>-->\n<ion-tabs>\n    <ion-tab-bar slot=\"bottom\">\n\n        <ion-tab-button tab=\"paradas\">\n            <ion-icon name=\"map\"></ion-icon>\n            <ion-label>Inicio</ion-label>\n        </ion-tab-button>\n        <ion-tab-button *ngIf=\"_authService?.usuario\"\n                        (click)=\"vaACambiarATabDePedidos()\"\n                        tab=\"pedidos\">\n            <ion-icon name=\"today\"></ion-icon>\n            <ion-label>Favoritos</ion-label>\n        </ion-tab-button>\n        <ion-tab-button *ngIf=\"!_authService?.usuario\" tab=\"seccion-menu\">\n            <ion-icon name=\"clipboard\"></ion-icon>\n            <ion-label>Rutas</ion-label>\n        </ion-tab-button>\n        <ion-tab-button *ngIf=\"_authService?.usuario\"\n                        tab=\"perfil\">\n            <ion-icon name=\"contact\"></ion-icon>\n            <ion-label>LogOut</ion-label>\n        </ion-tab-button>\n        <ion-tab-button *ngIf=\"!_authService?.usuario\"\n                        tab=\"login\">\n            <ion-icon name=\"contact\"></ion-icon>\n            <ion-label>Login</ion-label>\n        </ion-tab-button>\n    </ion-tab-bar>\n</ion-tabs>\n"

/***/ }),

/***/ "./src/app/submodulos/submodulo-auth0-movil/constantes/rutas-tab-auth.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/submodulos/submodulo-auth0-movil/constantes/rutas-tab-auth.ts ***!
  \*******************************************************************************/
/*! exports provided: RUTAS_TAB_AUTH */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RUTAS_TAB_AUTH", function() { return RUTAS_TAB_AUTH; });
const RUTAS_TAB_AUTH = [
    {
        path: 'perfil',
        children: [
            {
                path: '',
                loadChildren: '../submodulos/submodulo-auth0-movil/rutas/perfil/perfil.module#PerfilPageModule'
            }
        ]
    },
    {
        path: 'login',
        children: [
            {
                path: '',
                loadChildren: '../submodulos/submodulo-auth0-movil/rutas/login/login.module#LoginPageModule'
            }
        ]
    },
];


/***/ }),

/***/ "./src/app/tabs/tabs.module.ts":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.module.ts ***!
  \*************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs.page */ "./src/app/tabs/tabs.page.ts");
/* harmony import */ var _submodulos_submodulo_auth0_movil_constantes_rutas_tab_auth__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../submodulos/submodulo-auth0-movil/constantes/rutas-tab-auth */ "./src/app/submodulos/submodulo-auth0-movil/constantes/rutas-tab-auth.ts");








const routes = [
    {
        path: '',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"],
        children: [
            {
                path: 'locales-mapa',
                children: [
                    {
                        path: '',
                        loadChildren: '../rutas/locales-mapa/locales-mapa.module#LocalesMapaPageModule'
                    }
                ],
            },
            {
                path: 'paradas',
                children: [
                    {
                        path: '',
                        loadChildren: '../rutas/paradas/paradas.module#ParadasPageModule',
                    }
                ]
            },
            ..._submodulos_submodulo_auth0_movil_constantes_rutas_tab_auth__WEBPACK_IMPORTED_MODULE_7__["RUTAS_TAB_AUTH"],
            {
                path: '',
                redirectTo: 'locales-mapa',
                pathMatch: 'full'
            }
        ]
    },
];
let TabsPageModule = class TabsPageModule {
};
TabsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
    })
], TabsPageModule);



/***/ }),

/***/ "./src/app/tabs/tabs.page.scss":
/*!*************************************!*\
  !*** ./src/app/tabs/tabs.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3RhYnMvdGFicy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/tabs/tabs.page.ts":
/*!***********************************!*\
  !*** ./src/app/tabs/tabs.page.ts ***!
  \***********************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _submodulos_submodulo_auth0_movil_servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service */ "./src/app/submodulos/submodulo-auth0-movil/servicios/auth0/auth0.service.ts");



let TabsPage = class TabsPage {
    constructor(_authService) {
        this._authService = _authService;
    }
    ngOnInit() {
    }
    vaACambiarATabDePedidos() {
        // this._pedidoCarritoEmitterService.cargarUltimoPedido();
    }
};
TabsPage.ctorParameters = () => [
    { type: _submodulos_submodulo_auth0_movil_servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__["Auth0Service"] }
];
TabsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tabs',
        template: __webpack_require__(/*! raw-loader!./tabs.page.html */ "./node_modules/raw-loader/index.js!./src/app/tabs/tabs.page.html"),
        styles: [__webpack_require__(/*! ./tabs.page.scss */ "./src/app/tabs/tabs.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_submodulos_submodulo_auth0_movil_servicios_auth0_auth0_service__WEBPACK_IMPORTED_MODULE_2__["Auth0Service"]])
], TabsPage);



/***/ })

}]);
//# sourceMappingURL=tabs-tabs-module-es2015.js.map